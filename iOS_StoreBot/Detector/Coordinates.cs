﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector
{  
        public class  Coordinates
        {
        public  class Coords
            {
                public string Sign_In { get; set; }
                public string Other_opportunities { get; set; }
                public string Dont_Upgrade { get; set; }
                public string Done { get; set; }
                public string Install_button { get; set; }
                public string Enter_Install { get; set; }
                public string Always_Require { get; set; }
                public string Search_cancel_btn { get; set; }
                public string Sign_Out { get; set; }
                public string Deleting_Mode { get; set; }
                public string Delete_App { get; set; }
                public string Approve_Delete_1 { get; set; }
                public string Approve_Delete_2 { get; set; }
                public string Approve_Delete_3 { get; set; }
                public string After_login_result { get; set; }
                public string Search_bar_tap { get; set; }
                public string Search_text_field_tap { get; set; }
                public string Search_button_keyboard_tap { get; set; }
                public string Password_loginForm_text { get; set; }
                public string AppleID_loginForm_text { get; set; }
                public string Account_Button { get; set; }
                public string Account_continue_btn_RGB { get; set; }
                public string Search_text_field_aftertext_tap { get; set; }
                public string Flowbar_search_tap { get; set; }
                public string Search_back_tap { get; set; }
                public string Vpn_Btn_File { get; set; }
        }
        }
    }

