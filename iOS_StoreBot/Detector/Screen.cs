﻿using System;using System.Collections.Generic;using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Renci.SshNet;
using Renci.SshNet.Common;
using System.Linq;
using System.Diagnostics;
using iPhoneWorker;
using iPhoneWorker.Interfaces;
using iPhoneWorker.Services;


namespace iOS_StoreBot.Detector
{
    public class Screen
    {
        #region Variables
        private string _name;

        public Screen nextPossibleScreen;

        public static bool VpnMode = true;
        public static bool SpooferMode = true;
        protected List<string> AllowFeatures;
        protected List<string> BlockedFeatures;
        public static bool Pause = false;
        public static int phase = 0;
        public string AppIcon;
        public static string pattern = @".00";
        #endregion
        #region entering data
        public static string PathToScripts = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()))) + "\\iOS_StoreBot" + "\\pythonScripts";
        public static string PathToPython = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()))) + "\\iOS_StoreBot\\python\\python.exe";
        public static string PathToVpnFile = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()))) +"\\iOS_StoreBot" + "\\pythonScripts" + "\\indexvpn.ovpn";

        public static string IP_ssh_server;
        public static string _accountsJsonPath = Environment.CurrentDirectory + "\\accounts.json";
        public static string _coordsJsonPath = Environment.CurrentDirectory + "\\coords.json";
        public static string accId;
        public static int ipIndex = 0; // Индекс для перебора айпишников из полученного json списка в цикле.
        private ShellStream shellStream { get; set; }
        public static List<Accounts.AppleAcc> Accs;
        public static List<Coordinates.Coords> Coordinates;
        public static List<VpnIp._VpnIp> VpnIps;
        #endregion
        #region Python scripts calling
        public static string pyTap =
          PathToScripts + "\\pyTap.py";
        public static string pyTapWait15 =
         PathToScripts + "\\pyTapWait15.py";
        public static string pySwaipeDownApps =
        PathToScripts + "\\pySwaipeDownApps.py";
        public static string pyInsertText =
        PathToScripts + "\\pyInsertText.py";
        public static string pySwitchApp =
        PathToScripts + "\\pySwitchApp.py";
        public static string pyGetRgb =
        PathToScripts + "\\pyGetRgb.py";
        public static string pyGetImgCoords =
        PathToScripts + "\\pyGetImgCoords.py";
        public static string pyGetTextCoords =
        PathToScripts + "\\pyGetTextCoords.py";
        public static string pyRunCommand =
        PathToScripts + "\\pyRunCommand.py";
        public static string pyHideKeyboard =
        PathToScripts + "\\pyHideKeyboard.py";
        #endregion
        #region Path to screen images inside iPhone
        public static string SpaceQuestImg = "/private/var/storeimages/spacequestimg.png";
        public static string ChoiseFigureImg = "/private/var/storeimages/choisefigureimg.png";
        public static string GetButtonImg = "/private/var/storeimages/getbuttoning.png";
        public static string AccountLabel = "/private/var/storeimages/accountlabelaccountbutton.png";
        public static string AccountButton = "/private/var/storeimages/accountstorebuttonimg.png";

        public static string OpentButtonImg = "/private/var/storeimages/openbutton.png";
        public static string CloudInstallImg = "/private/var/storeimages/cloudinstallbutton.png";
        public static string OtherOptionsImg = "/private/var/storeimages/otheroptions.png";
        public static string OtherOptionLockImg = "/private/var/storeimages/OtherOptionLock.png";
        public static string MessageHomeScreenImg = "/private/var/storeimages/homemsgimg.png";
        public static string HomeAppStoreIconImg = "/private/var/storeimages/homeappstoreicon.png";
        public static string FlowBarTodayImg = "/private/var/storeimages/flowbartoday.png";
        public static string FlowBarGamesImg = "/private/var/storeimages/flowbargames.png";
        public static string FlowBarAppsImg = "/private/var/storeimages/flowbarapps.png";
        public static string FlowBarArcadeImg = "/private/var/storeimages/flowbararcade.png";
        public static string FlowBarSearchImg = "/private/var/storeimages/flowbarsearch.png";
        public static string UnactiveFlowBarSearchImg = "/private/var/storeimages/unactiveflowbarsearch.png";

        public static string AccountFieldLoginImg = "/private/var/storeimages/accounttextfieldlogin.png";
        public static string AccountFieldPasswordImg = "/private/var/storeimages/accounttextfieldpassword.png";
        public static string AccountDoneButtonImg = "/private/var/storeimages/accountdonebutton.png";
        public static string AccountUnactiveSignInImg = "/private/var/storeimages/accountunactivesignin.png";

        public static string StorePageSearchLabel = "/private/var/storeimages/storepagesearch.png";
        public static string StorePageTodayLabel = "/private/var/storeimages/storepagetoday.png";
        public static string StorePageSignOutLabel = "/private/var/storeimages/signoutactive.png";

        public static string StorePageCreateAppIdLabel = "/private/var/storeimages/createnewappleid.png";
        public static string StorePageTextFieldSearchLabel = "/private/var/storeimages/textfieldsearch.png";
        public static string StoreFieldPlaceHolder = "/private/var/storeimages/textfieldspaceholder.png";
        public static string StoreSearchMicrophone = "/private/var/storeimages/microphone.png";

        public static string StoreSearchBack = "/private/var/storeimages/appcardsearchback.png";
        public static string StoreAppStopInstall = "/private/var/storeimages/appcardstopinstall.png";
        public static string StoreAproveBtnInstall = "/private/var/storeimages/installbtnapprove.png";
        public static string StoreAproveBtnSignIn = "/private/var/storeimages/signinbtnaprove.png";

        public static string StoreAproveAlwaysRequire = "/private/var/storeimages/alwaysrequireaprove.png";
        public static string CancelSearch = "/private/var/storeimages/cancelsearch.png";
        public static string WhatsNewContinueBtn = "/private/var/storeimages/whatsnewcontinuebtn.png";
        public static string AccountSignInActive = "/private/var/storeimages/accountunactivesignin.png";
        public static string OpenBtnPush = "/private/var/storeimages/openbuttonPush.png";
        public static string btnSave = "/private/var/storeimages/btnSave.png";

        public static string DontAllow = "/private/var/storeimages/dontallow.png";
        public static string BtnLink = "/private/var/storeimages/btn_link.png";

        public static string BtnNotNow = "/private/var/storeimages/notnow.png";

        public static string MgsUdid = "/private/var/storeimages/mgs_udid.png";
        public static string MgsRand = "/private/var/storeimages/mgs_randudid.png";

        public static string MgsUniqueChip = "/private/var/storeimages/MgsUniqueChipId.png";
        public static string MgsMLB = "/private/var/storeimages/MgsMlb.png";
        public static string MgsDieID = "/private/var/storeimages/MgsDieID.png";
        public static string MgsChipId = "/private/var/storeimages/MgsChipId.png";

        public static string MgsWiFiAddressData = "/private/var/storeimages/mgs_wifiaddressdata.png";
        public static string MgsWiFiAddress = "/private/var/storeimages/mgs_wifiaddress.png";
        public static string MgsDeviceName = "/private/var/storeimages/mgs_devicename.png";
        public static string MgsComputerName = "/private/var/storeimages/mgs_computername.png";
        public static string MgsBluetooth = "/private/var/storeimages/mgs_bluetooth.png";
        public static string MgsUserDeviceName = "/private/var/storeimages/mgs_userassdevicename.png";
        public static string MgsUdidData = "/private/var/storeimages/mgs_udiddata.png";
        public static string MgsSerialNumber = "/private/var/storeimages/mgs_serialnumber.png";
        public static string MgsLabel = "/private/var/storeimages/mgs_label.png";

        public static string DeviceInfoBack = "/private/var/storeimages/DeviceInfoBack.png"; // <
        public static string UnactiveFlowBarSearch = "/private/var/storeimages/unactiveFlowBarSearch.png";

        public static string FilzaProfile  = "/private/var/storeimages/filza_profile.png";
        public static string FilzaOpenVpn = "/private/var/storeimages/filza_openVpnIcon.png";
        public static string OpenVPN_LabelImportProfile = "/private/var/storeimages/openVpn_LabelImportProfile.png";
        public static string OpenVPN_Delete = "/private/var/storeimages/openVpn_delete.png";

        public static string OpenVpn_radioBtnUnactive = "/private/var/storeimages/openVpn_radioBtnUnactive.png";
        public static string OpenVpn_allowBtn = "/private/var/storeimages/openVpn_allowBtn.png";
        public static string OpenVpn_addBtn2 = "/private/var/storeimages/openVpn_addBtn2.png";
        public static string OpenVpn_addBtn = "/private/var/storeimages/openVpn_addBtn.png";
        public static string OpenVpn_radioBtnActive = "/private/var/storeimages/openVpn_radioBtnActive.png";
        public static string OpenVpn_editBtn = "/private/var/storeimages/openVpn_editBtn.png";
        public static string OpenVpn_deleteProfileBtn = "/private/var/storeimages/openVpn_deleteProfileBtn.png";
        public static string OpenVpn_yesBtn = "/private/var/storeimages/openVpn_yesBtn.png";
        public static string OpenVpn_menuBtn = "/private/var/storeimages/openVpn_menuBtn.png";
        public static string OpenVpn_LabelProfiles = "/private/var/storeimages/openVpn_LabelProfiles.png";
        public static string OpenVpn_RetryBtn = "/private/var/storeimages/openVpn_RetryBtn.png";
        public static string OpenVpn_CancelBtn = "/private/var/storeimages/openVpn_CancelBtn.png";

        public static string AccountTerms = "/private/var/storeimages/label_conditions.png"; 
        public static string AccountTermsCancel = "/private/var/storeimages/accountTermsCancel.png";
        public static string HomeScreen02Switch = "/private/var/storeimages/hs02.png";
        public static string HomeScreenContacts = "/private/var/storeimages/hs_contacts.png";
       
        #endregion
        #region Change Region
        public static string RegionPhoneNumberImg = "/private/var/storeimages/region_phoneNumber.png";
        public static string RegionPhoneNumberCodeImg = "/private/var/storeimages/region_phoneCode.png";
        public static string RegionCityImg = "/private/var/storeimages/region_City.png";
        public static string RegionPostCodeImg = "/private/var/storeimages/region_postCode.png";
        public static string RegionStreetOptionalImg = "/private/var/storeimages/region_StreetOptional.png";
        public static string RegionBtnTermsAgree = "/private/var/storeimages/region_BtnTermsAgree.png";
        public static string RegionBtnAgree = "/private/var/storeimages/region_BtnAgree.png";
        public static string RegionSetLocaleGermany = "/private/var/storeimages/region_SetLocaleGermany.png";
        public static string RegionCountryScreenLabel = "/private/var/storeimages/region_CountryRegionScreenLabel.png";
        public static string RegionBtnChangeCountryOrRegion = "/private/var/storeimages/Region_BtnChangeCountryOrRegion.png";
        public static string RegionBtnOk = "/private/var/storeimages/Region_BtnOk.png";
        public static string RegionAccountSettings = "/private/var/storeimages/Region_AccountSettings.png";
        public static string RegionSettingsCountryRegion = "/private/var/storeimages/Region_SettingsCountryRegion.png";

        public static string RegionFormNone = "/private/var/storeimages/region_none.png";
        public static string RegionStreetRequired = "/private/var/storeimages/region_StreetRequired.png";
        public static string RegionPostCode = "/private/var/storeimages/region_postCode.png";
        public static string RegionCity = "/private/var/storeimages/region_City.png";
        public static string RegionPhoneCode = "/private/var/storeimages/region_phoneCode.png";
        public static string RegionPhoneNumber = "/private/var/storeimages/region_phoneNumber.png";
        public static string RegionNext = "/private/var/storeimages/region_next.png";

        #endregion
        public static string AppImg1 = "/private/var/storeimages/app1.png";
        public static string AppImg2 = "/private/var/storeimages/app2.png";
        public static string AppImg3 = "/private/var/storeimages/app3.png";
        public static string AppImg4 = "/private/var/storeimages/app4.png";

        public static TelnetConnection SocketInstance;

        public Screen(string name)
        {
            _name = name;
            AllowFeatures = new List<string>();
            BlockedFeatures = new List<string>();
            AppIcon = Form_Main.AppIcon;
            Pause = Form_Main.Pause;
            IP_ssh_server = Form_Main.IP_ssh_server;
           
            
            Phone phone = new Phone(IP_ssh_server, "root", "alpine");
            IPhoneJobService phoneJobService = new PhoneJobService(phone);    
            SocketInstance = TelnetConnection.Instance;
            //SocketInstance.StartSocketClient(IP_ssh_server);
            
            }

        public void AddAllowFeatures(string feature)
        {
            AllowFeatures.Add(feature);
        }
        public void AddBlockedFeatures(string feature)
        {
            BlockedFeatures.Add(feature);
        }
        public string Name
        {
            get { return _name; }
        }
        public bool DetectScreenByImage()
        {
            List<bool> approveList = new List<bool>();
            if (AllowFeatures.Count == 0)
            {
                throw new Exception("Screen class didn't have allowed features");
            }

            foreach (var feature in AllowFeatures)
            {
                List<string> fImage = SocketInstance.socketFindImage(feature);

                if (fImage[0].Contains("false")|| (fImage == null))
                {
                    approveList.Add(false);
                }
                else
                {
                    approveList.Add(true);
                }
            }

            if (approveList.Contains(true))
            {
                foreach (var feature in BlockedFeatures)
                {                    
                    var fImage = SocketInstance.socketFindImage(feature);
                    if (!fImage.Contains("false")) //!|| !fImage.Contains("")
                    {
                        approveList.Add(false);
                    }
                        /*
                        approveList.Add(false);
                        // approveList.Add(true);
                        // return false;
                    }
                    else
                    {
                        // approveList.Add(false);
                        approveList.Add(true);
                        return false;
                    }
                        */
                }
            }

            if (!approveList.Contains(false))
            {
                return true;
            }
            return false;
        }
        
         public virtual bool PerformActionOnTheScreen()
         {
             Form_Main.LogInConsole("Do actions...");
             return false;
         }
        /*
         public static void wait(int milliseconds)
         {
             System.Windows.Forms.Timer timer1 = new System.Windows.Forms.Timer();
             if (milliseconds == 0 || milliseconds < 0) return;

             timer1.Interval = milliseconds;
             timer1.Enabled = true;
             timer1.Start();
             timer1.Tick += (s, e) =>
             {
                 timer1.Enabled = false;
                 timer1.Stop();
             };
             while (timer1.Enabled)
             {
                 Application.DoEvents();
             }
         }

         public string Name
         {
             get { return _name; }
         }
         /*
         #region Python Functions
         public static void Tap(string x, string y)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()))) + "\\iOS_StoreBot\\python\\python.exe";
                 // 2) Provide script and arguments                      
                 var arg1 = (x.Replace(pattern, string.Empty));
                 var arg2 = (y.Replace(pattern, string.Empty));
                 psi.Arguments = $"\"{pyTap}\" \"{IP_ssh_server}\" \"{arg1}\" \"{arg2}\"";
                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     goto again;
                 }
             }
             else
             {
                 goto again;
             }
         }

         /// <summary>
         /// Скорректированный +тап.
         /// </summary>
         /// <param name="x"></param>
         /// <param name="y"></param>
         /// <param name="mx"> Минус от ИКС </param>
         /// <param name="my"> Минус от ИГРИК</param>
         public static void Tap(string x, string y, string mx, string my)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()))) + "\\iOS_StoreBot\\python\\python.exe";
                 // 2) Provide script and arguments                      

                 var arg1 = (x.Replace(pattern, string.Empty));
                 var arg2 = (y.Replace(pattern, string.Empty));
                 arg1 = (Convert.ToInt32(arg1) + Convert.ToInt32(mx)).ToString();
                 arg2 = (Convert.ToInt32(arg2) + Convert.ToInt32(my)).ToString();

                 psi.Arguments = $"\"{pyTap}\" \"{IP_ssh_server}\" \"{arg1}\" \"{arg2}\"";
                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     goto again;
                 }
             }
             else
             {
                 goto again;
             }
         }

         public static void TapWait15(string x, string y) // Исправить спагетти! *!
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = Path.GetDirectoryName(Path.GetDirectoryName(Path.GetDirectoryName(System.IO.Directory.GetCurrentDirectory()))) + "\\iOS_StoreBot\\python\\python.exe";
                 // 2) Provide script and arguments                      
                 var arg1 = (x.Replace(pattern, string.Empty));
                 var arg2 = (y.Replace(pattern, string.Empty));
                 psi.Arguments = $"\"{pyTapWait15}\" \"{IP_ssh_server}\" \"{arg1}\" \"{arg2}\"";
                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     goto again;
                 }
             }
             else
             {
                 goto again;
             }
         }
         public static void Swipe(int x1, int y1, int x2, int y2)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = PathToPython;
                 // 2) Provide script and arguments      
                 psi.Arguments = $"\"{pySwaipeDownApps}\" \"{IP_ssh_server}\" \"{x1}\" \"{y1}\" \"{x2}\" \"{y2}\"";

                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;

                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";

                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     // ConsoleBoxMain.AppendText("Script Error");
                     //   ConsoleBoxMain.AppendText(errors);
                     goto again;
                 }
             }
             else
             {
                 goto again;
             }
         }
         public static void InsertText(string text)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = PathToPython;
                 // 2) Provide script and arguments      
                 psi.Arguments = $"\"{pyInsertText}\" \"{IP_ssh_server}\" \"{text}\"";

                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;

                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";

                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     //    ConsoleBoxMain.AppendText("Script Error");
                     //    ConsoleBoxMain.AppendText(errors);
                     goto again;
                 }
                 //LogInConsole("Password in account form entered");
             }
             else
             {
                 goto again;
             }
         }
         public static void Switch(string appBoundle)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = PathToPython;
                 // 2) Provide script and arguments                      
                 //var arg1 = x.ToString();
                 //var arg2 = y.ToString();
                 psi.Arguments = $"\"{pySwitchApp}\" \"{IP_ssh_server}\" \"{appBoundle}\"";
                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     //   ConsoleBoxMain.BeginInvoke(
                     //                     (MethodInvoker)(() => ConsoleBoxPython.AppendText("\n" + "Script Error." + "\n")));
                     //   ConsoleBoxMain.BeginInvoke(
                     //                  (MethodInvoker)(() => ConsoleBoxMain.AppendText("\n" + errors + "\n")));

                     //ConsoleBoxMain.AppendText("Script Error");
                     //ConsoleBoxMain.AppendText(errors);
                     goto again;
                 }
             }
             else
             {
                 goto again;
             }
         }
         public static string GetRGB(int x, int y)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = PathToPython;
                 // 2) Provide script and arguments                      
                 var arg1 = x.ToString();
                 var arg2 = y.ToString();
                 psi.Arguments = $"\"{pyGetRgb}\" \"{IP_ssh_server}\" \"{arg1}\" \"{arg2}\"";
                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     Form_Main.LogInConsole("Script Error." + "\n" + errors);
                     //   ConsoleBoxMain.BeginInvoke(
                     //                     (MethodInvoker)(() => ConsoleBoxPython.AppendText("\n" + "Script Error." + "\n")));
                     //   ConsoleBoxMain.BeginInvoke(
                     //                  (MethodInvoker)(() => ConsoleBoxMain.AppendText("\n" + errors + "\n")));

                     //ConsoleBoxMain.AppendText("Script Error");
                     //ConsoleBoxMain.AppendText(errors);
                     // return errors;
                     goto again;
                 }
                 return results;
             }
             else
             {
                 goto again;
             }
         }
         public static string FindByImage(string AppName)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = PathToPython;
                 // 2) Provide script and arguments                     

                 psi.Arguments = $"\"{pyGetImgCoords}\" \"{IP_ssh_server}\" \"{AppName}\"";
                 //psi.Arguments = $"\"{pyGetImgCoords}\" \"{"192.168.0.147"}\" \"{AppName}\"";

                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     Form_Main.LogInConsole("Script Error." + "\n" + errors);
                     //    ConsoleBoxMain.BeginInvoke(
                     //                      (MethodInvoker)(() => ConsoleBoxPython.AppendText("\n" + "Script Error." + "\n")));

                     //     ConsoleBoxMain.BeginInvoke(
                     //                   (MethodInvoker)(() => ConsoleBoxMain.AppendText("\n" + errors + "\n")));

                     //ConsoleBoxMain.AppendText("Script Error");
                     //ConsoleBoxMain.AppendText(errors);
                     wait(2000);
                     Form_Main.LogInConsole("...Trying again.");
                     goto again;

                 }
                 process.Close();
                // Form_Main.LogInConsole("Img fragment founded.");
                 return (results);
             }
             else
             {
                 Form_Main.LogInConsole("FindImage Error. Trying again");
                 wait(2000);
                 goto again;

                 //return "error";
             }
         }
         public static string GenerateMACAddress()
         {
             var sBuilder = new StringBuilder();
             var r = new Random();
             int number;
             byte b;
             for (int i = 0; i < 6; i++)
             {
                 number = r.Next(0, 255);
                 b = Convert.ToByte(number);
                 if (i == 0)
                 {
                     b = setBit(b, 6); //--> set locally administered
                     b = unsetBit(b, 7); // --> set unicast 
                 }
                 if (i != 0)
                 {
                     sBuilder.Append(":");
                 }
                 sBuilder.Append(number.ToString("X2"));
             }
             return sBuilder.ToString().ToUpper();
         }
         private static byte setBit(byte b, int BitNumber)
         {
             if (BitNumber < 8 && BitNumber > -1)
             {
                 return (byte)(b | (byte)(0x01 << BitNumber));
             }
             else
             {
                 throw new InvalidOperationException(
                 "Der Wert für BitNumber " + BitNumber.ToString() + " war nicht im zulässigen Bereich! (BitNumber = (min)0 - (max)7)");
             }
         }
         private static byte unsetBit(byte b, int BitNumber)
         {
             if (BitNumber < 8 && BitNumber > -1)
             {
                 return (byte)(b | (byte)(0x00 << BitNumber));
             }
             else
             {
                 throw new InvalidOperationException(
                 "Der Wert für BitNumber " + BitNumber.ToString() + " war nicht im zulässigen Bereich! (BitNumber = (min)0 - (max)7)");
             }
         }
         public static string FindText(string x, string y, string width, string height, string text)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = PathToPython;
                 // 2) Provide script and arguments                      
                 // var arg1 = x.ToString();
                 // var arg2 = y.ToString();
                 //List<int> list; 
                 psi.Arguments = $"\"{pyGetTextCoords}\" \"{IP_ssh_server}\" \"{x}\" \"{y}\" \"{width}\" \"{height}\" \"{text}\"";
                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     Form_Main.LogInConsole("Script Error." + "\n" + errors);
                     //   ConsoleBoxMain.BeginInvoke(
                     //                     (MethodInvoker)(() => ConsoleBoxPython.AppendText("\n" + "Script Error." + "\n")));
                     //   ConsoleBoxMain.BeginInvoke(
                     //                  (MethodInvoker)(() => ConsoleBoxMain.AppendText("\n" + errors + "\n")));

                     //ConsoleBoxMain.AppendText("Script Error");
                     //ConsoleBoxMain.AppendText(errors);
                     // return errors;
                     goto again;
                 }
                 return results;
             }
             else
             {
                 goto again;
             }
         }  
         public static void ResetDeviceId()
         {
             Screen.Switch("com.navercorp.businessinsight.DeviceInfo");
             wait(2000);
             var img = FindByImage(DeviceInfoBack);
             if (img == "false\r\n")
             {
                 //Screen.Tap("22", "82");
                 Screen.Tap("35", "70");
             }
             wait(2000);
             Screen.Tap("156", "228");
             Form_Main.LogInConsole("Device ID UUID has ben reseted.");
         }
         public static void Respring()
         {

             Cmd("killall -9 SpringBoard");
             }
         public static string Cmd(string command)
         {
             again:
             if (Pause == false)
             {
                 // 1) Create Process Info
                 ProcessStartInfo psi = new ProcessStartInfo();
                 psi.FileName = PathToPython;
                 // 2) Provide script and arguments                     

                 psi.Arguments = $"\"{pyRunCommand}\" \"{IP_ssh_server}\" \"{command}\"";
                 //psi.Arguments = $"\"{pyGetImgCoords}\" \"{"192.168.0.147"}\" \"{AppName}\"";

                 // 3) Process configuration
                 psi.UseShellExecute = false;
                 psi.CreateNoWindow = true;
                 psi.RedirectStandardOutput = true;
                 psi.RedirectStandardError = true;
                 // 4) Execute process and get output
                 var errors = "";
                 var results = "";
                 Process process = new Process();
                 //using (Process process = Process.Start(psi))
                 using (process = Process.Start(psi))
                 {
                     errors = process.StandardError.ReadToEnd();
                     results = process.StandardOutput.ReadToEnd();
                 }

                 if (errors != "")
                 {
                     Form_Main.LogInConsole("Script Error." + "\n" + errors);

                     wait(2000);
                     // Form_Main.LogInConsole("...Trying again.");
                     //goto again;

                 }
                 process.Close();
                 // Form_Main.LogInConsole(results);
                 // Form_Main.LogInConsole("Command complete");
                 return (results);
             }
             else
             {
                 goto again;
                 return "error";
             }

         }
         #endregion
         */
        #region Vpn Profile 
        //public static int[] restrictedIp = new int[100];      
        public static List<int> restrictedIp = new List<int>();
        public static string HideKeyboard()
        {
            again:
            if (Pause == false)
            {
                // 1) Create Process Info
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = PathToPython;
                // 2) Provide script and arguments                     

                psi.Arguments = $"\"{pyHideKeyboard}\" \"{IP_ssh_server}\"";
                //psi.Arguments = $"\"{pyGetImgCoords}\" \"{"192.168.0.147"}\" \"{AppName}\"";

                // 3) Process configuration
                psi.UseShellExecute = false;
                psi.CreateNoWindow = true;
                psi.RedirectStandardOutput = true;
                psi.RedirectStandardError = true;
                // 4) Execute process and get output
                var errors = "";
                var results = "";
                Process process = new Process();
                //using (Process process = Process.Start(psi))
                using (process = Process.Start(psi))
                {
                    errors = process.StandardError.ReadToEnd();
                    results = process.StandardOutput.ReadToEnd();
                }

                if (errors != "")
                {
                    Form_Main.LogInConsole("Script Error." + "\n" + errors);

                    SocketInstance.socketUsleep(2000);
                }
                process.Close();
                return (results);
            }
            else
            {
                goto again;
                return "error";
            }

        }
        public static void ChangeFileVpnIp()
        {
            
            Random rnd = new Random();
            ipIndex = rnd.Next(0, VpnIps.Count());
            if(restrictedIp.Contains(ipIndex))
            {
                ipIndex++;
            }    
            else
            {
                restrictedIp.Add((ipIndex));
            }

            string[] text = File.ReadAllLines(PathToVpnFile);
            //text[3] = "remote " + VpnIps[Convert.ToInt32(ipIndex)].IP + "1194";
            text[3] = "remote " + VpnIps[(ipIndex)].IP + " " + "1194";
            File.WriteAllText(PathToVpnFile, String.Join("\n", text));
            Form_Main.LogInConsole("Ip changed: " + VpnIps[Convert.ToInt32(ipIndex)].IP);
            //ipIndex++;

        }
        public static void UploadVpnFile()
        {       
            string host = IP_ssh_server.ToString();
            const string username = "root";
            const string password = "alpine";
            const string workingdirectory = "/private/var/vpn/";
            string uploadfile = PathToVpnFile; // @"c:\yourfilegoeshere.txt";

            Console.WriteLine("Creating client and connecting");
            using (var client = new SftpClient(host, 22, username, password))
            {
                client.Connect();
                Console.WriteLine("Connected to {0}", host);

                client.ChangeDirectory(workingdirectory);
                Console.WriteLine("Changed directory to {0}", workingdirectory);

                var listDirectory = client.ListDirectory(workingdirectory);
                Console.WriteLine("Listing directory:");
                foreach (var fi in listDirectory)
                {
                    Console.WriteLine(" - " + fi.Name);
                }

                using (var fileStream = new FileStream(uploadfile, FileMode.Open))
                {
                    Console.WriteLine("Uploading {0} ({1:N0} bytes)", uploadfile, fileStream.Length);
                    client.BufferSize = 4 * 1024; // bypass Payload error large files
                    client.UploadFile(fileStream, Path.GetFileName(uploadfile));
                }

                Form_Main.LogInConsole("VPN profile uploaded");
            }
        }      
        public static void ResetVpnProfile()
        {
            ChangeFileVpnIp();
            UploadVpnFile();
            Form_Main.LogInConsole("Vpn file replaced. Switch Filza");
            SocketInstance.socketSwitchApp("com.tigisoftware.Filza");
        }
        public static void FileZilaSetNewVpnProfile()
        {
            ResetVpnProfile();
            Thread.Sleep(10000);
            var img = SocketInstance.socketFindImage(FilzaProfile);
            //var spliter = img.Split();
            while (true)
            {
                if (img.Contains("false"))
                {
                    Form_Main.LogInConsole("Do swipe");
                    SocketInstance.socketSwipeDown(490, 800, 450, 470, 3, 10);
                    SocketInstance.socketUsleep(3000);
                    Thread.Sleep(3000);
                    img = SocketInstance.socketFindImage(FilzaProfile);
                    SocketInstance.socketUsleep(3000);
                    Thread.Sleep(3000);

                }
                else
                {
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Form_Main.LogInConsole("Vpn profile taped");
                    SocketInstance.socketUsleep(2000);
                    break;
                }
            }

            Thread.Sleep(2000);

            img = SocketInstance.socketFindImage(FilzaOpenVpn);
            //SocketInstance.socketTap(Convert.ToInt32(img[0]), Convert.ToInt32(img[1])); //90,822
      
            Form_Main.LogInConsole("Filza opened file by VPN.");
            SocketInstance.socketUsleep(2000);
        }
    }
    #endregion
}

    
