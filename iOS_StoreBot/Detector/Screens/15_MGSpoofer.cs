﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _15_MGSpoofer : Screen
    {
        public _15_MGSpoofer()
          : base(typeof(_15_MGSpoofer).Name)
        {
            
            if (VpnMode == true)
            {
                nextPossibleScreen = new _17_openVpn();
            }
            else
            {
                nextPossibleScreen = new _02_HomeScreen_2();
            }

            AddAllowFeatures(MgsLabel);
           //AddBlockedFeatures(StorePageTextFieldSearchLabel);
            AddBlockedFeatures(StorePageSearchLabel);
            AddBlockedFeatures(AccountLabel);
        }
        public override bool PerformActionOnTheScreen()
        {
            try
            {              

                SocketInstance.socketSwipeDown(380, 200, 264, 690, 3, 10);
                SocketInstance.socketUsleep(2000);
                //udid
                {
                    Form_Main.LogInConsole("Randomize Unqie Device ID");
                    Spoof(MgsUdid);
                    MgsRand();
                    SocketInstance.socketUsleep(2000);
                    Form_Main.LogInConsole("Unqie Device ID randomized");
                }
                // serial
                {
                    Form_Main.LogInConsole("Randomize Serial Number");
                    Spoof(MgsSerialNumber);
                    MgsRand();
                    Form_Main.LogInConsole("Serial Number randomized");
                }
                //Udid data
                {
                    Spoof(MgsUdidData);
                    MgsRand();
                    SocketInstance.socketUsleep(2000);
                    Form_Main.LogInConsole("Udid Data randomized");
                
                }

                //User Assigned Device Name
                {
                    Form_Main.LogInConsole("Randomize Assigned Device Name");
                    Spoof(MgsUserDeviceName);
                    MgsRand();
                    SocketInstance.socketUsleep(2000);
                }
                //Bluetooth Address
                {
                    Form_Main.LogInConsole("Randomize Bluetooth Address");
                    Spoof(MgsBluetooth);
                    SocketInstance.socketUsleep(2000);
                    SocketInstance.socketInsertText(GenerateMACAddress().ToString());
                    SocketInstance.socketUsleep(2000);
                    SocketInstance.socketTap("280", "600");
                    
                    //MgsRand();
                    /*
                    imgCoords = FindByImage(MgsRand);
                    spliter = imgCoords.Split();
                    Tap((spliter[0].Replace(pattern, string.Empty)),
                        (spliter[1].Replace(pattern, string.Empty)));
                    */
                    Form_Main.LogInConsole("Bluetooth Address randomized");
                    SocketInstance.socketUsleep(2000);
                }               
                //Device Name
                {
                    Form_Main.LogInConsole("Randomize Device Name");
                    Spoof(MgsDeviceName);
                    MgsRand();
                    SocketInstance.socketUsleep(2000);
                    Form_Main.LogInConsole("Device Name randomized");
                }
                //Wifi Address
                {
                    Form_Main.LogInConsole("Randomize Wifi Address");
                    Spoof(MgsWiFiAddressData);
                    SocketInstance.socketUsleep(2000);
                    
                    SocketInstance.socketInsertText(GenerateMACAddress().ToString());
                    SocketInstance.socketUsleep(2000);
                    SocketInstance.socketTap("280", "600");

                    Form_Main.LogInConsole("Wifi Address randomized");
                }
                SocketInstance.socketUsleep(2000);
                SocketInstance.socketSwipeDown(264, 690, 380, 200, 3, 10);
                SocketInstance.socketUsleep(2000);
                //Wifi Address Data
                {
                    Form_Main.LogInConsole("Randomize Wifi Address Data");
                    var imgCoords = SocketInstance.socketFindImage(MgsWiFiAddressData);
                    //var spliter = imgCoords.Split();
                    SocketInstance.socketTap((imgCoords[0]),(imgCoords[1]));

                    SocketInstance.socketUsleep(2000);

                    SocketInstance.socketInsertText(GenerateMACAddress().ToString());
                    SocketInstance.socketUsleep(2000);
                    SocketInstance.socketTap("342", "616");
                    Form_Main.LogInConsole("Wifi Address Data randomized");
                }

                SocketInstance.socketUsleep(2000);
                SocketInstance.socketSwipeDown(264, 690, 380, 200, 3, 10);
                SocketInstance.socketUsleep(2000);

                //MgsUniqueChip
                {
                    //wait(2000);
                    Form_Main.LogInConsole("Randomize MgsUniqueChip");
                    Spoof(MgsUniqueChip);
                    MgsRand();
                    SocketInstance.socketUsleep(2000);
                    Form_Main.LogInConsole("MgsUniqueChip randomized");
                }

                //MgsMlb
                {
                    Form_Main.LogInConsole("Randomize MgsMlb");
                    Spoof(MgsMLB);
                    MgsRand();
                    Form_Main.LogInConsole("MgsMlb randomized");
                    SocketInstance.socketUsleep(2000);
                }

                //MgsDieID
                {
                    Form_Main.LogInConsole("Randomize MgsDieID");
                    Spoof(MgsDieID);
                    MgsRand();
                    Form_Main.LogInConsole("MgsDieID randomized");
                    SocketInstance.socketUsleep(2000);
                }

                //MgsChipId
                {
                    Form_Main.LogInConsole("Randomize MgsChipId");
                    Spoof(MgsChipId);
                    MgsRand();

                    Form_Main.LogInConsole("DMgsChipId randomized");
                    SocketInstance.socketUsleep(2000);
                }

                SocketInstance.socketSwipeDown(350, 900, 400, 300, 3, 10);
                SocketInstance.socketUsleep(2000);

                //Computer Name
                {
                    Form_Main.LogInConsole("Randomize Computer Name");
                    Spoof(MgsComputerName);
                    MgsRand();

                    Form_Main.LogInConsole("Assigned Device Name randomized");
                    SocketInstance.socketUsleep(2000);
                }


                SocketInstance.socketCmd("aditfake");
                Form_Main.LogInConsole("Adv Faked");
                Form_Main.LogInConsole("MGS ids randomized. Reset Device Id UUID");
                SocketInstance.ResetDeviceId();               
                Form_Main.LogInConsole("Reloading SpringBoard");

                SocketInstance.socketRespring();
                SocketInstance.socketUsleep(3000);
                //  Screen.Switch("net.joebruce.RespringHelper");
               // wait(2000);
                //Screen.Tap("340", "342");
              //  wait(2000);

                Form_Main.LogInConsole("SpringBoard reloaded.");

                SocketInstance.socketSwitchApp("net.openvpn.connect.app");

                /*
                if (VpnMode == true)
                {
                    Form_Main.LogInConsole("VPN mode on.");
                    SocketInstance.socketSwitchApp("net.openvpn.connect.app");
                }
                else
                {
                    Form_Main.LogInConsole("VPN mode off");
                    SocketInstance.socketSwitchApp("com.apple.AppStore");
                }
                */

            }
            catch (Exception e)
            {
                Form_Main.LogInConsole(" ERROR MGSpoofer, " + e);
               // goto again;
            }
            return true;
        }

        private void Spoof(string Spoof)
        {
            bool swipedUp = false;
            spoof:
            var img = SocketInstance.socketFindImage(Spoof);
            if (!img.Contains("false"))
            {
                //var spliter = img.Split();
                SocketInstance.socketTap((img[0]),(img[1]));
            }

            else if(swipedUp = false)
            {
                swipedUp = true;
                SocketInstance.socketSwipeUp(380, 200, 264, 690, 3, 10);
                Form_Main.LogInConsole("Swipe Up. Trying again.");
                swipedUp = true;
                goto spoof;
            }

            else
            {
                SocketInstance.socketSwipeDown(350, 900, 400, 300, 3, 10);
                Form_Main.LogInConsole("Swipe Down. Trying again.");
                goto spoof;
            }
            swipedUp = false;
        }

        private void MgsRand()
        {
            Form_Main.LogInConsole("Trying to randomize...");
            randomMgs:
            try
            {
                var imgCoords = SocketInstance.socketFindImage(Screen.MgsRand);
                //var spliter = imgCoords.Split();
                SocketInstance.socketTap((imgCoords[0]), (imgCoords[1]));
            }
            catch(Exception e)
            {
                Form_Main.LogInConsole("Errow! Randomize btn not found! Trying again...");

                goto randomMgs;
            }
        }
        public static string GenerateMACAddress()
        {
            var sBuilder = new StringBuilder();
            var r = new Random();
            int number;
            byte b;
            for (int i = 0; i < 6; i++)
            {
                number = r.Next(0, 255);
                b = Convert.ToByte(number);
                if (i == 0)
                {
                    b = setBit(b, 6); //--> set locally administered
                    b = unsetBit(b, 7); // --> set unicast 
                }
                if (i != 0)
                {
                    sBuilder.Append(":");
                }
                sBuilder.Append(number.ToString("X2"));
            }
            return sBuilder.ToString().ToUpper();
        }
        private static byte setBit(byte b, int BitNumber)
        {
            if (BitNumber < 8 && BitNumber > -1)
            {
                return (byte)(b | (byte)(0x01 << BitNumber));
            }
            else
            {
                throw new InvalidOperationException(
                "Der Wert für BitNumber " + BitNumber.ToString() + " war nicht im zulässigen Bereich! (BitNumber = (min)0 - (max)7)");
            }
        }
        private static byte unsetBit(byte b, int BitNumber)
        {
            if (BitNumber < 8 && BitNumber > -1)
            {
                return (byte)(b | (byte)(0x00 << BitNumber));
            }
            else
            {
                throw new InvalidOperationException(
                "Der Wert für BitNumber " + BitNumber.ToString() + " war nicht im zulässigen Bereich! (BitNumber = (min)0 - (max)7)");
            }
        }
    }
}
