﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _20_TermsAccount : Screen
    {
        public _20_TermsAccount()
           : base(typeof(_20_TermsAccount).Name)
        {
            AddAllowFeatures(AccountTerms);
            AddBlockedFeatures(AccountLabel);
            AddBlockedFeatures(OtherOptionsImg);
            AddBlockedFeatures(StorePageSignOutLabel);
            AddBlockedFeatures(StorePageTextFieldSearchLabel);

            AddBlockedFeatures(GetButtonImg);
            AddBlockedFeatures(CloudInstallImg);
            AddBlockedFeatures(MessageHomeScreenImg);
            AddBlockedFeatures(StorePageSignOutLabel);

        }
        public override bool PerformActionOnTheScreen()
        {
            var img = SocketInstance.socketFindImage(CancelSearch);
            SocketInstance.socketTap(img[0],img[1]);
            return true;
        }
    }
}