﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _10_AppStoreSignOutAccount : Screen
    {
        public _10_AppStoreSignOutAccount()
          : base(typeof(_10_AppStoreSignOutAccount).Name)
        {
            nextPossibleScreen = new _08_AppStoreAccount();

            AddAllowFeatures(StorePageSignOutLabel);
            AddBlockedFeatures(StorePageCreateAppIdLabel);
        }
        public override bool PerformActionOnTheScreen()
        {
            again:
            try
            {
                if (phase == 2)
                {
                    var img = SocketInstance.socketFindImage(StorePageSignOutLabel);
                    //var spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    return true;
                }
                if (phase == 1)
                {
                    var img = SocketInstance.socketFindImage(StorePageSignOutLabel);
                    if (!img.Contains("false\r\n"))
                    {
                        Form_Main.LogInConsole("Login checked positive");
                        img = SocketInstance.socketFindImage(AccountDoneButtonImg);
                        //var spliter = img.Split();
                        SocketInstance.socketTap((img[0]), (img[1]));
                        accId = (Convert.ToInt32(accId)+1).ToString();
                        return true;
                    }
                    else
                    {
                        Form_Main.LogInConsole("ERROR login checking");
                    }
                }
            }
            catch (Exception e)
            {
                Form_Main.LogInConsole(" Trying again, " + e);
                goto again;
            }
            return true;
        }
    }
}
