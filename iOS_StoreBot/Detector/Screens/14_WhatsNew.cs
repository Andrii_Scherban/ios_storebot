﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    public class _14_WhatsNew: Screen
    {
        public _14_WhatsNew()
      : base(typeof(_14_WhatsNew).Name)
        {
            // AddAllowFeatures(StorePageSearchLabel);
           // AddAllowFeatures(AppIcon);
            AddAllowFeatures(WhatsNewContinueBtn);
            
            AddBlockedFeatures(CloudInstallImg);
            AddBlockedFeatures(StorePageTextFieldSearchLabel);
            AddBlockedFeatures(OpentButtonImg);
            AddBlockedFeatures(AccountLabel);
            // AddAllowFeatures(FlowBarSearchImg);
            // AddAllowFeatures(UnactiveFlowBarSearchImg);
            /*
             AddBlockedFeatures(FlowBarTodayImg);
             AddBlockedFeatures(FlowBarGamesImg);
             AddBlockedFeatures(FlowBarAppsImg);
             AddBlockedFeatures(FlowBarArcadeImg);
          */

        }

        public override bool PerformActionOnTheScreen()
        {
            var img = SocketInstance.socketFindImage(WhatsNewContinueBtn);
            //var spliter = img.Split();
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap((img[0]), (img[1]));

            SocketInstance.socketUsleep(2000);    
            return true;
        }
    }
}
