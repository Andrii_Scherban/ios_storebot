﻿using System;
using System.Threading;

namespace iOS_StoreBot.Detector.Screens
{
    public class _01_HomeScreen_1 : Screen
    {
        
        public _01_HomeScreen_1()
        : base(typeof(_01_HomeScreen_1).Name)
        {            
            AddAllowFeatures(MessageHomeScreenImg);
            AddAllowFeatures(HomeAppStoreIconImg);
            
            AddBlockedFeatures(HomeScreenContacts);
            AddBlockedFeatures(GetButtonImg);
            AddBlockedFeatures(OpentButtonImg);
            AddBlockedFeatures(CloudInstallImg);
            //AddBlockedFeatures(Screen.AppImg1);
            //AddBlockedFeatures(Screen.AppImg2);
            //AddBlockedFeatures(Screen.AppImg3);
            //AddBlockedFeatures(Screen.AppImg4);


        }

        public override bool PerformActionOnTheScreen()
        {
            again:
            try
            {
                

                if (phase == 0)
                {
                    SocketInstance.socketRespring();
                    Thread.Sleep(2000);
                    //  SocketInstance.socketSwitchApp("com.apple.AppStore");
                    SocketInstance.socketSwitchApp("com.tonyk7.mgspoofhelper");
                    /*
                    if (Screen.VpnMode == true)
                    {

                        SocketInstance.socketSwitchApp("net.openvpn.connect.app");
                        //Switch("net.openvpn.connect.app");
                    }
                    else if (Screen.SpooferMode == true)
                    {
                        SocketInstance.socketSwitchApp("com.tonyk7.mgspoofhelper");
                    }
                    else
                    {
                        SocketInstance.socketSwitchApp("com.apple.AppStore");
                    }
                    */
                    //Screen.Switch("com.tonyk7.mgspoofhelper");
                }

                if (phase == 2)
                {
                    Form_Main.LogInConsole("HomeScreen_1 phase 2");
                    Form_Main.LogInConsole("Swipe to 02_HomeScreen");
                    SocketInstance.socketSwipeDown(566, 730, 100, 790, 2, 10);


                }
                return true;
            }

            catch (Exception e)
            {
                Form_Main.LogInConsole("Error inside 01_HomeScreen " + e);
                SocketInstance.socketUsleep(2000);
                Form_Main.LogInConsole("Trying again");
                goto again;
            }

            }
    }
}
