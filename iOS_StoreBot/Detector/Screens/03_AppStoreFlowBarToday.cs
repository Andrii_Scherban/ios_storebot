﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
   public class _03_AppStoreToday : Screen
    {
        //private static readonly bool FlowBarTodayActive = true;

        public _03_AppStoreToday()
            : base(typeof(_03_AppStoreToday).Name)
        {
            //AddAllowFeatures(FlowBarTodayImg);
            AddAllowFeatures(StorePageTodayLabel);
            /*
            AddBlockedFeatures(FlowBarGamesImg);
            AddBlockedFeatures(FlowBarAppsImg);
            AddBlockedFeatures(FlowBarArcadeImg);
            AddBlockedFeatures(FlowBarSearchImg);
        */
        }

        public override bool PerformActionOnTheScreen()
        {
            var img = SocketInstance.socketFindImage(UnactiveFlowBarSearch);
            //var spliter = img.Split();
            SocketInstance.socketTap((img[0]),(img[1]));
            return true;
        }
    }
}
