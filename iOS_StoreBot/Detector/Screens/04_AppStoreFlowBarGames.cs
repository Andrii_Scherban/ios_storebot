﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
   public class _04_AppStoreGames: Screen
    {
        public _04_AppStoreGames()
           : base(typeof(_04_AppStoreGames).Name)
        {
            AddAllowFeatures(FlowBarGamesImg);
            
            AddBlockedFeatures(FlowBarTodayImg);
            AddBlockedFeatures(FlowBarAppsImg);
            AddBlockedFeatures(FlowBarArcadeImg);
            AddBlockedFeatures(FlowBarSearchImg);
        
            }
        public override bool PerformActionOnTheScreen()
        {
            var spliter = Coordinates[0].Flowbar_search_tap.Split();
            SocketInstance.socketTap((spliter[0]),(spliter[1]));
            return true;
        }
    }
}
