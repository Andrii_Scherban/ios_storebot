﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _02_HomeScreen_2 : Screen
    {
        
        public _02_HomeScreen_2()
       : base(typeof(_02_HomeScreen_2).Name)
        {
            //nextPossibleScreen = ScreenDetector._listScreens[0];
            //_15_MGSpoofer();HomeScreen02
            //AddAllowFeatures(HomeScreen02);
            AddAllowFeatures(MessageHomeScreenImg);
            AddAllowFeatures(HomeAppStoreIconImg);
            //AddAllowFeatures(HomeAppStoreIconImg);
            AddBlockedFeatures(GetButtonImg);
            AddBlockedFeatures(OpentButtonImg);
            AddBlockedFeatures(CloudInstallImg);
            
        }
        public override bool PerformActionOnTheScreen()
        { again:
            try
            {
                if (phase == 0)
                {
                    //Switch("com.apple.AppStore");
                    SocketInstance.socketSwitchApp("com.tonyk7.mgspoofhelper"); //***!
                                                               //Switch("net.openvpn.connect.app");

                    /*
                    if (Screen.VpnMode == true)
                    {
                        SocketInstance.socketSwitchApp("net.openvpn.connect.app");
                    }
                    else if (Screen.SpooferMode == true)
                    {
                        SocketInstance.socketSwitchApp("com.tonyk7.mgspoofhelper");
                    }
                    else
                    {
                        SocketInstance.socketSwitchApp("com.apple.AppStore");
                    }
                    */
                }                   
                if (phase == 2)
                {
                    again2:
                    Form_Main.LogInConsole("Finding app on 02_HomeScreen...");
                    var imgCoords = SocketInstance.socketFindImage(AppIcon);                     
                    {

                        var spliter = Screen.Coordinates[0].Delete_App.Split();
                        // TapWait15((spliter[0].Replace(pattern, string.Empty)),(spliter[1].Replace(pattern, string.Empty)));
                        SocketInstance.socketTap(((spliter[0].Replace(pattern, string.Empty))),
                            (spliter[1].Replace(pattern, string.Empty)), 1);

                        Form_Main.LogInConsole("Deleting app");
                        SocketInstance.socketUsleep(2000);

                        spliter = Coordinates[0].Approve_Delete_1.Split();
                        SocketInstance.socketTap((spliter[0]),(spliter[1]));
                        Form_Main.LogInConsole("Approve Delete");
                        SocketInstance.socketUsleep(2000);


                        spliter = Coordinates[0].Approve_Delete_2.Split();
                        SocketInstance.socketTap((spliter[0]),(spliter[1]));
                        Form_Main.LogInConsole("Approve Delete2");
                        SocketInstance.socketUsleep(2000);

                        spliter = Coordinates[0].Approve_Delete_3.Split();
                        SocketInstance.socketTap((spliter[0]),(spliter[1]));
                        Form_Main.LogInConsole("Approve Delete3");
                        SocketInstance.socketUsleep(2000);

                        Form_Main.LogInConsole("Deleting check");
                        //imgCoords = FindByImage(AppIcon);

                       
                        Thread.Sleep(2000);
                        
                            imgCoords = SocketInstance.socketFindImage(AppIcon);
                        if (imgCoords.Contains("false"))
                        {
                            Screen.phase = 0; //*!!!
                            Form_Main.LogInConsole("App successfully deleted. ");
                            Form_Main.LogInConsole("Return top phase 0, install phase.");
                            Form_Main.LogInConsole("Respring");

                            // SocketInstance.socketUsleep(2000);
                            //Screen.Respring();
                            SocketInstance.socketRespring();
                            SocketInstance.socketUsleep(2000);
                            SocketInstance.socketSwitchApp("com.tonyk7.mgspoofhelper");
                            //Switch("net.openvpn.connect.app");                            
                        }
                        else
                        {
                            Form_Main.LogInConsole("App NOT deleted. Trying again... ");
                            goto again2;
                        }

                    }
                    return true;
                }
                return (true);
            }
            catch(Exception e)
            {
                Form_Main.LogInConsole(" Trying again, " + e);
                goto again;
            }
        }
    }
}
        
    
    
