﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _19_RegionChange : Screen
    {
        public _19_RegionChange()
            : base(typeof(_19_RegionChange).Name)
        {
            AddAllowFeatures(RegionAccountSettings);
            //AddAllowFeatures(HomeAppStoreIconImg);
            // AddBlockedFeatures(GetButtonImg);
            // AddBlockedFeatures(OpentButtonImg);
            // AddBlockedFeatures(CloudInstallImg);
            // AddBlockedFeatures(AccountLabel);
        }
        public override bool PerformActionOnTheScreen()
        {
            WayToRegionChangeForm();
            SocketInstance.socketUsleep(2000);
           // InsertFormData();
          //  wait(2000);
            return true;
        }

        public static void WayToRegionChangeForm()
        {
            Form_Main.LogInConsole("Push region Country/Region");

            var img = SocketInstance.socketFindImage(RegionSettingsCountryRegion);
            //var spliter = img.Split();
            SocketInstance.socketTap((img[0]), (img[1]));

            Form_Main.LogInConsole("Insert Password");
            SocketInstance.socketUsleep(5000);
            SocketInstance.socketInsertText(Screen.Accs[Int32.Parse(Screen.accId)].Password.ToString());
            SocketInstance.socketUsleep(2000);

            Form_Main.LogInConsole("Push OK");
            img = SocketInstance.socketFindImage(RegionBtnOk);
            if (!img.Contains("false"))
            {
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]), (img[1]));
            }
            SocketInstance.socketUsleep(5000);

            Form_Main.LogInConsole("Push button Change Country or Region");
            img = SocketInstance.socketFindImage(RegionBtnChangeCountryOrRegion);
            //spliter = img.Split();
            SocketInstance.socketTap((img[0]), (img[1]));

            //__
            img = SocketInstance.socketFindImage(RegionSetLocaleGermany);
            int indexSwipes = 0;
            Form_Main.LogInConsole("Trying to find German locale");
            while (true)
            {
                if (img.Contains("false"))
                {
                    SocketInstance.socketSwipeDown(350, 962, 350, 334, 3, 10);
                    // wait(1000);
                    img = SocketInstance.socketFindImage(RegionSetLocaleGermany);
                    indexSwipes++;
                    Form_Main.LogInConsole("Do swipe №" + " " + indexSwipes);
                }
                else
                {
                    break;
                }
            }
            Form_Main.LogInConsole("German Local was found");
            //spliter = img.Split();
            SocketInstance.socketTap((img[0]), (img[1]));
            //__

            SocketInstance.socketUsleep(3000);

            Form_Main.LogInConsole("Pushd Term&Conditions Agree");

            img = SocketInstance.socketFindImage(RegionBtnTermsAgree);
            //spliter = img.Split();
            SocketInstance.socketTap((img[0]), (img[1]));

            SocketInstance.socketUsleep(2000);

            Form_Main.LogInConsole("Agree readings 2");
            img = SocketInstance.socketFindImage(RegionBtnTermsAgree);
            //spliter = img.Split();
            SocketInstance.socketTap((img[0]), (img[1]));

            SocketInstance.socketUsleep(5000);
            //InsertFormData();
        }

        /*
        public static void InsertFormData()
        {
            wait(5000);
            Form_Main.LogInConsole("RegionFormNone");
            var img = SocketInstance.socketFindImage(RegionFormNone);
            //var spliter = img.Split();
            while (img.Contains("false"))
            {
                Form_Main.LogInConsole("Trying again...");
                img = SocketInstance.socketFindImage(RegionSetLocaleGermany);            
            }
            wait(2000);
            SocketInstance.socketTap(Convert.ToInt32(img[0]), Convert.ToInt32(img[1]));

            SocketInstance.socketSwipeDown(350, 962, 350, 334, 3, 10);

            wait(2000);
            img = SocketInstance.socketFindImage(RegionStreetRequired);
            //spliter = img.Split();
            Tap((spliter[0].Replace(pattern, string.Empty)),
                  (spliter[1].Replace(pattern, string.Empty)),
                  "300","0");
            InsertText("Invalidenstrabe");
            HideKeyboard();
           
            img = FindByImage(RegionPostCode);
            spliter = img.Split();
            Tap((spliter[0].Replace(pattern, string.Empty)),
                  (spliter[1].Replace(pattern, string.Empty)),
                  "300", "0");
            wait(2000);
            InsertText("10178");
            HideKeyboard();

            img = FindByImage(RegionCity);
            spliter = img.Split();
            Tap((spliter[0].Replace(pattern, string.Empty)),
                  (spliter[1].Replace(pattern, string.Empty)),
                  "300", "0");
            wait(2000);
            InsertText("Berlin");
            HideKeyboard();

            img = FindByImage(RegionPhoneCode);
            spliter = img.Split();
            Tap((spliter[0].Replace(pattern, string.Empty)),
                  (spliter[1].Replace(pattern, string.Empty)));
            wait(2000);
            InsertText("049");
            HideKeyboard();

            img = FindByImage(RegionPhoneNumber);
            spliter = img.Split();
            Tap((spliter[0].Replace(pattern, string.Empty)),
                  (spliter[1].Replace(pattern, string.Empty)));
            wait(2000);
            InsertText("30901820");
            HideKeyboard();
            
            img = FindByImage(RegionNext);
            spliter = img.Split();
            Tap((spliter[0].Replace(pattern, string.Empty)),
                  (spliter[1].Replace(pattern, string.Empty)));
            wait(2000);
        }
*/
       
    }
}
