﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    public class _11_StoreAppCard: Screen
    {
        public _11_StoreAppCard()
      : base(typeof(_11_StoreAppCard).Name)
        {
            nextPossibleScreen = new _13_StoreAppCardInstalled();

            // AddAllowFeatures(StorePageSearchLabel);
            // AddAllowFeatures(AppIcon);
            AddAllowFeatures(GetButtonImg);
            
            AddBlockedFeatures(CloudInstallImg);
            AddBlockedFeatures(StorePageTextFieldSearchLabel);
            //AddBlockedFeatures(OpentButtonImg);
            
            // AddAllowFeatures(FlowBarSearchImg);
            // AddAllowFeatures(UnactiveFlowBarSearchImg);
            /*
             AddBlockedFeatures(FlowBarTodayImg);
             AddBlockedFeatures(FlowBarGamesImg);
             AddBlockedFeatures(FlowBarAppsImg);
             AddBlockedFeatures(FlowBarArcadeImg);
          */

        }

        public override bool PerformActionOnTheScreen()
        { 
            again:
            try
            {
                SocketInstance.socketSwipeUp(380, 200, 264, 690, 3, 10);
                Thread.Sleep(2000);
                Form_Main.LogInConsole("Install process start");
                SocketInstance.socketUsleep(3000);

                var img = SocketInstance.socketFindImage(GetButtonImg);
                //var spliter = img.Split();
                SocketInstance.socketTap((img[0]),(img[1]));

                SocketInstance.socketUsleep(2000);
                //password save
                img = SocketInstance.socketFindImage(GetButtonImg);

                if (!img.Contains("false"))
                {
                    Form_Main.LogInConsole("Password saving form. Btn SAVE pushd");
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                }
              
                //
                Form_Main.LogInConsole("Aprovement...");

                while (true)
                {
                    img = SocketInstance.socketFindImage(StoreAproveBtnInstall);
                    if (!img.Contains("false"))
                    {
                        // spliter = img.Split();
                        SocketInstance.socketTap((img[0]),(img[1]));
                        Form_Main.LogInConsole("Approvement Install Tapped.");
                        SocketInstance.socketUsleep(5000);
                        break;
                    }
                    else
                    {
                        Form_Main.LogInConsole("Waiting for Install Approvement Form.");
                    }
                }
               
                SocketInstance.socketInsertText(Accs[Int32.Parse(accId)].Password.ToString());

                img = SocketInstance.socketFindImage(StoreAproveBtnSignIn);
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]), (img[1]));

                Form_Main.LogInConsole("Password passed. Waiting for A.R.");
                while (true)
                {
                    img = SocketInstance.socketFindImage(StoreAproveAlwaysRequire);
                    if (!img.Contains("false"))
                    {
                        //spliter = img.Split();
                        SocketInstance.socketTap((img[0]),(img[1]));
                        Form_Main.LogInConsole("A.R. tapped. ");
                        Form_Main.LogInConsole("Install Complete");
                        SocketInstance.socketUsleep(5000);
                        break;
                    }
                    else
                    {
                        Form_Main.LogInConsole("Waiting for A.R.");
                    }
                }
                img = SocketInstance.socketFindImage(StoreAppStopInstall);
                //spliter = img.Split();
                while (!img.Contains("false"))
                {
                    Form_Main.LogInConsole("Installing...");
                    SocketInstance.socketUsleep(3000);
                    img = SocketInstance.socketFindImage(StoreAppStopInstall);
                    SocketInstance.socketUsleep(3000);
                }

                return true;
            }
            catch (Exception e)
            {
                Form_Main.LogInConsole(" Trying again, " + e);
                goto again;
            }
        }
    }
}
