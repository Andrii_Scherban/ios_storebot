﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    public class _12_StoreAppCardCloud : Screen
    {
        public _12_StoreAppCardCloud()
      : base(typeof(_12_StoreAppCardCloud).Name)
        {
            nextPossibleScreen = new _13_StoreAppCardInstalled();

            AddAllowFeatures(CloudInstallImg);

            AddBlockedFeatures(StorePageTextFieldSearchLabel);
            AddBlockedFeatures(OpentButtonImg);
            AddBlockedFeatures(StorePageTextFieldSearchLabel);
        }

        public override bool PerformActionOnTheScreen()
        {
            //SocketInstance.socketSwipeUp(380, 200, 264, 690, 3, 10);
            Thread.Sleep(2000);
            again:
            try
            {
                var img = SocketInstance.socketFindImage(CloudInstallImg);
                //var spliter = img.Split();
                SocketInstance.socketTap((img[0]),(img[1]));

                return true;
            }
            catch (Exception e)
            {
                Form_Main.LogInConsole(" Trying again, " + e);
                goto again;
            }
        }
        }
    }

