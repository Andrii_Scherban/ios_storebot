﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace iOS_StoreBot.Detector.Screens
{
    public class _08_AppStoreAccount : Screen
    {       
        public _08_AppStoreAccount()
           : base(typeof(_08_AppStoreAccount).Name)
        {
            if (phase == 0)
            {
                nextPossibleScreen = new _09_OtherOptions();
            }
            if (phase == 1)
            {
                nextPossibleScreen = new _02_HomeScreen_2();
            }
            if (phase == 2)
            {
                nextPossibleScreen = new _02_HomeScreen_2();
            }

            AddAllowFeatures(AccountLabel);
            AddBlockedFeatures(StorePageSignOutLabel);
            AddBlockedFeatures(OtherOptionsImg);
            AddBlockedFeatures(StorePageSignOutLabel);
            AddBlockedFeatures(StorePageTextFieldSearchLabel);

            //AddBlockedFeatures(AppIcon);
            AddBlockedFeatures(AccountTerms);
            AddBlockedFeatures(GetButtonImg);
            AddBlockedFeatures(CloudInstallImg);
            AddBlockedFeatures(MessageHomeScreenImg);
            AddBlockedFeatures(StorePageSignOutLabel);
            //AddBlockedFeatures(OpentButtonImg);
        }
        public override bool PerformActionOnTheScreen()
        {
            
            again:
            try
            {
                if (phase == 0)
                {
                    Form_Main.LogInConsole("____________________");
                    Form_Main.LogInConsole("New iterration. Account Id:" + " " + accId);
                    Form_Main.LogInConsole("Ip ID: " + Screen.ipIndex);
                    Form_Main.LogInConsole("Ip: " + VpnIps[Convert.ToInt32(ipIndex)].IP);
                    Form_Main.LogInConsole("____________________");
                    Form_Main.labelId.BeginInvoke(
                                       (MethodInvoker)(() => Form_Main.labelId.Text = accId.ToString()));

                    Thread.Sleep(2000);

                    var img = SocketInstance.socketFindImage(AccountTerms);

                     if (!img.Contains("false"))
                    {
                        Form_Main.LogInConsole("Terms screen founded. Pushd Cancel");
                        img = SocketInstance.socketFindImage(AccountTermsCancel);
                        SocketInstance.socketTap(img[0], img[1]);
                        Thread.Sleep(2000);

                        img = SocketInstance.socketFindImage(AccountButton);
                        //var spliter = accountFieldLoginCoords.Split();
                        SocketInstance.socketTap(img[0], img[1]);

                    }
                    var accountFieldLoginCoords = SocketInstance.socketFindImage(AccountFieldLoginImg);


                    //var spliter = accountFieldLoginCoords.Split();

                    //All Of Apple At Yuor fingerprints "Cancel" btn after 1 iterations


                    SocketInstance.socketTap((accountFieldLoginCoords[0]), (accountFieldLoginCoords[1]));
                    Thread.Sleep(2000);
                    SocketInstance.socketInsertText(Accs[Int32.Parse(accId)].Login.ToString());
                    Thread.Sleep(2000);
                    var accountFieldPasswordCoords = SocketInstance.socketFindImage(AccountFieldPasswordImg);
                    //spliter = accountFieldPasswordCoords.Split();
                    SocketInstance.socketTap((accountFieldPasswordCoords[0]), (accountFieldPasswordCoords[1]));

                    SocketInstance.socketInsertText(Accs[Int32.Parse(accId)].Password.ToString());

                    var accountSignIn = SocketInstance.socketFindImage(AccountSignInActive);
                    //spliter = accountSignIn.Split();
                    SocketInstance.socketTap((accountSignIn[0]), 
                        (accountSignIn[1]));


                   var spliter = Coordinates[0].Account_continue_btn_RGB.Split();

                   /*
                    var rgbAfterLoginResult = SocketInstance.GetRGB((Convert.ToInt32(spliter[0]), Convert.ToInt32(spliter[1]));
                    if (rgbAfterLoginResult.Contains("255"))
                    {
                        spliter = Coordinates[0].Other_opportunities.Split();
                        SocketInstance.socketTap(Convert.ToInt32(spliter[0]), Convert.ToInt32(spliter[1]));
                        Form_Main.LogInConsole("News screen detected");
                    }
                    */
                    SocketInstance.socketUsleep(3000);

                    Form_Main.LogInConsole("Login aproving...");
                    phase = 1;
                    return true;
                }
                if (phase == 1)
                {
                    var img = SocketInstance.socketFindImage(AccountDoneButtonImg);
                    //var spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Form_Main.LogInConsole("Login complete. App install phase");
                    //Form_Main.LogInConsole("Respring...");
                    //SocketInstance.socketRespring();
                    //SocketInstance.socketUsleep(2000);

                    Thread.Sleep(2000);

                    SocketInstance.socketSwitchApp("com.apple.AppStore");
                    Thread.Sleep(2000);
                    //SocketInstance.socketUsleep(2000);
                }
                if (phase == 2)
                {
                   
                    Thread.Sleep(2000);
                    var img = SocketInstance.socketFindImage(AccountDoneButtonImg);
                    // var img = SocketInstance.socketFindImage(AccountDoneButtonImg);
                    //var spliter = img.Split();
                    SocketInstance.socketTap((img[0]),
                       (img[1]));
                    Form_Main.LogInConsole("Sign out done");
                    Form_Main.LogInConsole("Deleting app...");
                    SocketInstance.socketSwitchApp("com.apple.springboard");
                    return true;
                }
                //Screen.phase = 1;
                return true;
            }
            catch (Exception e)
            {
                Form_Main.LogInConsole(" Trying again, " + e);
                goto again;
            }
        }

    }
}
