﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _05_AppStoreApps: Screen
    {
        public _05_AppStoreApps()
           : base(typeof(_05_AppStoreApps).Name)
        {
            
            AddAllowFeatures(FlowBarAppsImg);
            /*
            AddBlockedFeatures(FlowBarTodayImg);
            AddBlockedFeatures(FlowBarGamesImg);
            AddBlockedFeatures(FlowBarArcadeImg);
            AddBlockedFeatures(FlowBarSearchImg);
        */
            }
        public override bool PerformActionOnTheScreen()
        {
            var spliter = Coordinates[0].Flowbar_search_tap.Split();
            SocketInstance.socketTap((spliter[0]), (spliter[1]));
            return true;
        }
    }
}
