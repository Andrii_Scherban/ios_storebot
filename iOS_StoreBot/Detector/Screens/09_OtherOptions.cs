﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    public class _09_OtherOptions : Screen
    {
        public _09_OtherOptions()
         : base(typeof(_09_OtherOptions).Name)
        {
            //nextPossibleScreen = new _08_AppStoreAccount();

            AddAllowFeatures(OtherOptionsImg);
            AddBlockedFeatures(MgsLabel);
        }
        public override bool PerformActionOnTheScreen()
        {
            //if (phase == 0)
            {
                again:
                try
                {
                    var img = SocketInstance.socketFindImage(OtherOptionsImg);
                    if (!img.Contains("false"))
                    {
                        Form_Main.LogInConsole("Other options label was found. Make tap.");

                        //SocketInstance.socketTap((img[0]), (Convert.ToInt32(img[1])).ToString());
                       
                        SocketInstance.socketTap(img[0], img[1]);
                        //SocketInstance.socketTap("330", "1274");

                        SocketInstance.socketUsleep(2000);

                        var spliter = Coordinates[0].Dont_Upgrade.Split();
                        SocketInstance.socketTap((spliter[0]), (spliter[1]));
                    }
                    else
                    {
                        Form_Main.LogInConsole("Other options label NOT FOUND. Trying again.");
                        SocketInstance.socketUsleep(3000);
                        goto again;
                    }

                    return true;
                }
                catch (Exception e)
                {
                    Form_Main.LogInConsole(" Trying again, " + e);
                    goto again;
                }
            }
        }
    }
}
