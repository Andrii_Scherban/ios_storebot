﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _07_AppStoreSearch : Screen
    {
        public _07_AppStoreSearch()
       : base(typeof(_07_AppStoreSearch).Name)
        {
            nextPossibleScreen = new _08_AppStoreAccount();

            if (phase == 0)
            {
                nextPossibleScreen = new _08_AppStoreAccount();

            }
            if (phase == 1)
            {
                nextPossibleScreen = new _11_StoreAppCard();
            }
                // AddAllowFeatures(StorePageSearchLabel);

                //AddAllowFeatures(StorePageTextFieldSearchLabel);
                //AddAllowFeatures(StoreFieldPlaceHolder);
                //AddAllowFeatures(StoreSearchMicrophone);
                AddAllowFeatures(StorePageSearchLabel);

            AddBlockedFeatures(OtherOptionsImg);
            //AddBlockedFeatures(OtherOptionLockImg);
            AddBlockedFeatures(MgsLabel);
            AddBlockedFeatures(OpentButtonImg);
            AddBlockedFeatures(AccountLabel);
            AddBlockedFeatures(BtnLink);
            AddBlockedFeatures(OpenVpn_menuBtn);
            
            // AddAllowFeatures(UnactiveFlowBarSearchImg);
            /*
             AddBlockedFeatures(FlowBarTodayImg);
             AddBlockedFeatures(FlowBarGamesImg);
             AddBlockedFeatures(FlowBarAppsImg);
             AddBlockedFeatures(FlowBarArcadeImg);
          */

        }

        public override bool PerformActionOnTheScreen()
        {
            again:
            try
            {
                if (phase == 0)
                {
                    var accountFieldLoginCoords = SocketInstance.socketFindImage(AccountButton);                   
                    SocketInstance.socketTap((accountFieldLoginCoords[0]),
                        (accountFieldLoginCoords[1]));

                }
                if (phase == 1)
                {
                    Thread.Sleep(2000);

                    var img = SocketInstance.socketFindImage(StorePageTextFieldSearchLabel);     
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Thread.Sleep(2000);
                    SocketInstance.socketInsertText(Form_Main.AppKeyword);
                    Thread.Sleep(2000);
                    //SocketInstance.socketUsleep(2000);                   

                    img = SocketInstance.socketFindImage(BtnNotNow);
                    if (!img.Contains("false"))
                    {
                        //spliter = img.Split();
                        SocketInstance.socketTap((img[0]),(img[1]));
                        Form_Main.LogInConsole("Dictation not ennabled. Pushd button Not Now");
                    }

                    //
                    Thread.Sleep(2000);
                    img = SocketInstance.socketFindImage(StoreFieldPlaceHolder);
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1])); //, "0", "10");

                    SocketInstance.socketUsleep(2000);


                   var spliter = Coordinates[0].Search_button_keyboard_tap.Split();
                    // SocketInstance.socketTap(("620"), ("1284"));
                    SocketInstance.socketTap((spliter[0]), (spliter[1]));
                    SocketInstance.socketUsleep(2000);                  

                    img = SocketInstance.socketFindImage(AppIcon);
                    int indexSwipes = 0;
                    Form_Main.LogInConsole("Trying to find app by icon");
                    while (true)
                    {
                        if (img.Contains("false"))
                        {
                            SocketInstance.socketSwipeDown(350, 962, 350, 334, 1, 10);
                            // wait(1000);
                            img = SocketInstance.socketFindImage(AppIcon);
                            indexSwipes++;
                            Form_Main.LogInConsole("Do swipe №" + " " + indexSwipes);
                        }
                        else
                        {
                            break;
                        }
                    }
                    Form_Main.LogInConsole("App was found.");
                    //if (Convert.ToInt32(y) > 1100)
                    {
                        // Form_Main.LogInConsole("App behind Flow Bar. Swipe once more.");
                        SocketInstance.socketSwipeDown(350, 962, 350, 750, 3, 10);
                        SocketInstance.socketUsleep(2000);

                        img = SocketInstance.socketFindImage(AppIcon);
                        //spliter = img.Split();
                        SocketInstance.socketTap((img[0]), (img[1]));
                    }

                    SocketInstance.socketUsleep(2000);

                    //*! ГОВНОКОД! ИСПРАВИТЬ!!!

                    /*
                    string y2 = (spliter[1].Replace(pattern, string.Empty));
                    int yy = Convert.ToInt32(y2);
                    int yyy = yy - 155;

                    wait(2000);
                    Tap(spliter[0], (yyy.ToString()));
                    wait(2000);
                    */

                    //SocketInstance.socketSwipeDown(400, 800, 400, 1200, 3, 10);
                    Form_Main.LogInConsole("App icon on search list was taped.");

                }
                if (phase == 2)
                {
                    Thread.Sleep(2000);
                    var accountFieldLoginCoords = SocketInstance.socketFindImage(AccountButton);
                    if (!accountFieldLoginCoords.Contains("false"))
                    {
                        SocketInstance.socketTap((accountFieldLoginCoords[0]),
                            (accountFieldLoginCoords[1]));
                    }
                    SocketInstance.socketSwitchApp("com.apple.springboard");
                }
                return true;
            }
            catch (Exception e)
            {
                Form_Main.LogInConsole(" Trying again, " + e);
                goto again;
            }
        }
    }
}
