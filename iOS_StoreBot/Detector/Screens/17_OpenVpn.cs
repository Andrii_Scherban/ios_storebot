﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    public class _17_openVpn : Screen
    {
        public _17_openVpn()
     : base(typeof(_17_openVpn).Name)
        {
            nextPossibleScreen = new _07_AppStoreSearch();

            //AddAllowFeatures(OpenVPN_LabelImportProfile);
            AddAllowFeatures(OpenVpn_menuBtn);

            AddBlockedFeatures(StorePageTextFieldSearchLabel);
            AddBlockedFeatures(GetButtonImg);
            AddBlockedFeatures(AccountLabel);
        }
        // 1 - install
        // 2 - uninstall
        public override bool PerformActionOnTheScreen()
        {
            //1234
            Thread.Sleep(2000);
           var spliter = Coordinates[0].Vpn_Btn_File.Split();
            SocketInstance.socketTap((spliter[0]), (spliter[1]));
            again:

            SocketInstance.socketUsleep(2000);
            var imgCoords = SocketInstance.socketFindImage(OpenVpn_radioBtnUnactive);
            if (!imgCoords.Contains("false"))
            //    if (Screen.phase == 2) //uninstall, disconnect vpn 
            {
                while (!imgCoords.Contains("false"))
                {
                    Form_Main.LogInConsole("Unactive Profiles detected. Start Deleting vpn Profiles");
               
                   var img = SocketInstance.socketFindImage(OpenVpn_editBtn);
                   //var spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Form_Main.LogInConsole("Edit button pushed");

                    SocketInstance.socketUsleep(2000);

                   img = SocketInstance.socketFindImage(OpenVpn_deleteProfileBtn);
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]),(img[1]));
                    Form_Main.LogInConsole("Profile deleted");

                    SocketInstance.socketUsleep(2000);

                    img = SocketInstance.socketFindImage(OpenVpn_yesBtn);
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Form_Main.LogInConsole("Yes button pushd");
                    imgCoords = SocketInstance.socketFindImage(OpenVpn_radioBtnUnactive);
                }
            }

            //Thre

            imgCoords = SocketInstance.socketFindImage(OpenVpn_radioBtnActive);
            if (!imgCoords.Contains("false"))
            //    if (Screen.phase == 2) //uninstall, disconnect vpn 
            {
                Form_Main.LogInConsole("Active Profile detected. Start Deleting vpn Profile");
                var img = SocketInstance.socketFindImage(OpenVpn_radioBtnActive);
                //ar spliter = img.Split();
                SocketInstance.socketTap((img[0]),(img[1]));

                SocketInstance.socketUsleep(3000);

                img = SocketInstance.socketFindImage(OpenVpn_editBtn);
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]),(img[1]));
                Form_Main.LogInConsole("Edit button pushed");

                SocketInstance.socketUsleep(2000);

                img = SocketInstance.socketFindImage(OpenVpn_deleteProfileBtn);
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]), (img[1]));
                Form_Main.LogInConsole("Profile deleted");

                SocketInstance.socketUsleep(2000);

                img = SocketInstance.socketFindImage(OpenVpn_yesBtn);
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]), (img[1]));
                Form_Main.LogInConsole("Yes button pushd");

                Thread.Sleep(2000);
                spliter = Coordinates[0].Vpn_Btn_File.Split();
                SocketInstance.socketTap((spliter[0]), (spliter[1]));
                Thread.Sleep(2000);
                spliter = Coordinates[0].Vpn_Btn_File.Split();
                SocketInstance.socketTap((spliter[0]), (spliter[1]));

            }

          
            // else
            {
                Form_Main.LogInConsole("Set up New VPN profile by Filza");
                FileZilaSetNewVpnProfile();
                Form_Main.LogInConsole("Filza done with new profile");
                SocketInstance.socketUsleep(2000);

                Thread.Sleep(2000);

                var img = SocketInstance.socketFindImage(OpenVpn_addBtn);
               // var spliter = img.Split();
                SocketInstance.socketTap((img[0]), (img[1]));
               Thread.Sleep(2000);
                Form_Main.LogInConsole("Add btn pushed");
                SocketInstance.socketUsleep(2000);
                Thread.Sleep(2000);

                img = SocketInstance.socketFindImage(OpenVpn_addBtn2);
                //spliter = img.Split();
                
                SocketInstance.socketTap((img[0]), (img[1]));
                Thread.Sleep(2000);
                Form_Main.LogInConsole("Add btn 2 pushd");
                //
                SocketInstance.socketUsleep(2000);
                img = SocketInstance.socketFindImage(OpenVpn_allowBtn);
                if (!img.Contains("false"))
                {
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Form_Main.LogInConsole("Allow btn pushd");
                }
                //

                Thread.Sleep(3000);

                img = SocketInstance.socketFindImage(OpenVpn_radioBtnUnactive);
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]),(img[1]));
                Form_Main.LogInConsole("Unactive radio button pushd");
                SocketInstance.socketUsleep(5000);

                Form_Main.LogInConsole("Returning to cycle.");
               SocketInstance.socketSwitchApp("com.apple.AppStore");
                Form_Main.LogInConsole("Check continue button.");
                img = SocketInstance.socketFindImage(WhatsNewContinueBtn);
                if (!img.Contains("false"))
                {
                    Form_Main.LogInConsole("Continue button founded.");
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Form_Main.LogInConsole("Taped continue button.");
                }
                else
                {
                    Form_Main.LogInConsole("Continue not founded.");
                }

                //accId = ((Convert.ToInt32(accId)) + 1).ToString();
                //Form_Main.LogInConsole("Returning to phase #0");
                //phase 0
                //Screen.phase = 0;

                return (true);

                //Screen.Switch("com.tonyk7.mgspoofhelper");
                //Form_Main.LogInConsole("Going to spoofer.");
            }

            return true;
        }
    }
}
