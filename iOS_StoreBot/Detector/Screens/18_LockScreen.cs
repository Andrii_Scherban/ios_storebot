﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _18_LockScreen : Screen
    {
        //1 - 170, 500
        //9 - 586, 664
        //4 - 164, 660

        public _18_LockScreen()
         : base(typeof(_18_LockScreen).Name)
        {
            AddAllowFeatures(MgsLabel);
            //AddBlockedFeatures(StorePageTextFieldSearchLabel);
        }
        public override bool PerformActionOnTheScreen()
        {
            SocketInstance.socketTap("170", "500");
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap("578", "906");
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap("578", "906");
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap("164", "660");
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap("170", "500");
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap("164", "660");
            return true;
        }

        public static bool EnterKey()
        {
            /*
            SocketInstance.socketTap(170, 500);
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap(578, 906);
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap(578, 906);
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap(164, 660);
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap(170, 500);
            SocketInstance.socketUsleep(2000);
            SocketInstance.socketTap(164, 660);
            SocketInstance.socketUsleep(2000);
           */
            return true;
        }
    }
}
