﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    public class _13_StoreAppCardInstalled : Screen
    {
        public _13_StoreAppCardInstalled()
      : base(typeof(_13_StoreAppCardInstalled).Name)
        {
            nextPossibleScreen = new _08_AppStoreAccount();
            AddAllowFeatures(OpentButtonImg);

            AddBlockedFeatures(StorePageTextFieldSearchLabel);
            AddBlockedFeatures(GetButtonImg);
            AddBlockedFeatures(AccountLabel);
        }

        public override bool PerformActionOnTheScreen()
        {
            try
            {
                Form_Main.LogInConsole("App Installed");

                var img = SocketInstance.socketFindImage(OpenBtnPush);
                //var spliter = img.Split();
                SocketInstance.socketTap((img[0]),(img[1]));

                Form_Main.LogInConsole("App opened");
                SocketInstance.socketUsleep(2000);
                Form_Main.LogInConsole("Notifications check.");

                img = SocketInstance.socketFindImage(DontAllow);
                //spliter = img.Split();
                if (!img.Contains("false"))
                {
                    Form_Main.LogInConsole("Notifications was found. Tap Don't Allow.");
                    SocketInstance.socketTap((img[0]),(img[1]));
                }
                else
                {
                    Form_Main.LogInConsole("Notifications not found.");
                }
                var spliter = Coordinates[0].Enter_Install.Split();
                SocketInstance.socketTap((spliter[0]),(spliter[1]));
                Form_Main.LogInConsole("Do tap inside app");
                SocketInstance.socketUsleep(2000);

                SocketInstance.socketSwipeDown(566, 730, 40, 790, 3, 10);
                Form_Main.LogInConsole("Do swipe inside app");
                SocketInstance.socketUsleep(2000);

                Form_Main.LogInConsole("Wait 30s");
                SocketInstance.socketUsleep(5000);
                Form_Main.LogInConsole("...20s");
                SocketInstance.socketUsleep(10000);

                Form_Main.LogInConsole("...10s");
                SocketInstance.socketUsleep(10000);

                Form_Main.LogInConsole("Done. Switch back to AppStore");
                SocketInstance.socketUsleep(2000);
                SocketInstance.socketSwitchApp("com.apple.AppStore");

                Form_Main.LogInConsole("Check continue button.");
                SocketInstance.socketUsleep(3000);
                img = SocketInstance.socketFindImage(WhatsNewContinueBtn);
                if (!img.Contains("false"))
                {
                    Form_Main.LogInConsole("Continue button founded.");
                    //spliter = img.Split();
                    SocketInstance.socketTap((img[0]), (img[1]));
                    Form_Main.LogInConsole("Taped continue button.");
                }
                else
                {
                    Form_Main.LogInConsole("Continue not founded.");
                }

                Form_Main.LogInConsole("Store Search back.");
                img = SocketInstance.socketFindImage(StoreSearchBack);
                
                SocketInstance.socketTap((img[0]), (img[1]));

                Screen.phase = 2;
                Form_Main.LogInConsole("Set phase: 2 - logout & delete");
                SocketInstance.socketUsleep(2000);
               
                Thread.Sleep(2000);
                
                img = SocketInstance.socketFindImage(CancelSearch);
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]), (img[1]));
                Thread.Sleep(2000);

                img = SocketInstance.socketFindImage(AccountButton);
                //spliter = img.Split();
                SocketInstance.socketTap((img[0]), (img[1]));
                Thread.Sleep(2000);

                // Switch("com.apple.springboard");
                return true;
            }
            catch (Exception e)
            {
                Form_Main.LogInConsole(" ERROR AppCardInstalled, " + e);
                //goto again;
            }
            return true;
        }
    }
}
