﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iOS_StoreBot.Detector.Screens
{
    class _06_AppStoreArcade: Screen
    {
        public _06_AppStoreArcade()
          : base(typeof(_06_AppStoreArcade).Name)
        {            
            AddAllowFeatures(FlowBarArcadeImg);
            /*
            AddBlockedFeatures(FlowBarTodayImg);
            AddBlockedFeatures(FlowBarGamesImg);
            AddBlockedFeatures(FlowBarAppsImg);
            AddBlockedFeatures(FlowBarSearchImg);  
        */
            }
        public override bool PerformActionOnTheScreen()
        {
            var spliter = Coordinates[0].Flowbar_search_tap.Split();
            SocketInstance.socketTap((spliter[0]),(spliter[1]));
            return true;
        }
    }
}
