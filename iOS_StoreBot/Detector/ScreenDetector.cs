﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;

namespace iOS_StoreBot.Detector.Screens
{
    class ScreenDetector
    {
        private static ScreenDetector _instanceScreenDetector;
        public static List<Screen> _listScreens;

        public static ScreenDetector Instance
        {
            get
            {
                if (_instanceScreenDetector == null)
                    _instanceScreenDetector = new ScreenDetector();
                return _instanceScreenDetector;
            }
        }

        private ScreenDetector()
        {
            InitScreens();
        }



        public void InitScreens()
        {
            _listScreens = new List<Screen>();
           
            _listScreens.Add(new _01_HomeScreen_1());
            _listScreens.Add(new _02_HomeScreen_2());
           
            _listScreens.Add(new _15_MGSpoofer());
            _listScreens.Add(new _07_AppStoreSearch());
            _listScreens.Add(new _17_openVpn());

            //_listScreens.Add(new _19_RegionChange());           
            //_listScreens.Add(new _03_AppStoreToday());
            //_listScreens.Add(new _04_AppStoreGames());
            //_listScreens.Add(new _05_AppStoreApps());
            //_listScreens.Add(new _06_AppStoreArcade());

            _listScreens.Add(new _07_AppStoreSearch());

            _listScreens.Add(new _10_AppStoreSignOutAccount());

            _listScreens.Add(new _08_AppStoreAccount());
            _listScreens.Add(new _20_TermsAccount());
            _listScreens.Add(new _09_OtherOptions());
           
            _listScreens.Add(new _11_StoreAppCard());
            _listScreens.Add(new _12_StoreAppCardCloud());
            _listScreens.Add(new _13_StoreAppCardInstalled());

            _listScreens.Add(new _14_WhatsNew());
            //_listScreens.Add(new _15_MGSpoofer());
      
            
        }
        public void DetectScreen()
        {
            try
            {
                List<bool> count = new List<bool>();
                foreach (var screen in _listScreens)
                {
                    Form_Main.LogInConsole("...Detecting");
                    again:
                    if (Form_Main.Pause == false)
                    {
                       if (screen.DetectScreenByImage() != false) // Detect from all feauters
                    // if(SynchronousSocketClient.Instance.socketFindImage() != "false")
                        {
                            // MessageBox.Show("Detect screen: " + screen.GetType());
                            // Do function
                            //Form_Main.LogInConsole(screen.GetType().ToString());
                            Form_Main.LogInConsole(screen.Name);
                            screen.PerformActionOnTheScreen();

                           // if (screen.nextPossibleScreen != null)
                            {
                                //screen.nextPossibleScreen
                            }

                            //*!!!
                            
                            //
                            count.Add(true);
                            break;
                        }
                        else
                        {
                            count.Add(false);
                            // MessageBox.Show("Screen Unknown");
                        }
                    }
                    else
                    {
                        Thread.Sleep(10000);
                        goto again;
                    }
                        Thread.Sleep(2000);
                }

                if (!count.Contains(true))
                {
                    Form_Main.LogInConsole("SCREEN UNKNOWN. Trying again...");
                }

                // return new ScreenUnknow();
            }
            catch (Exception e)
            {
                Form_Main.LogInConsole("Error on screen detection! " + e);
            }
        }

        public void DetectNextScreen(Screen screen)
        {
            if (screen.nextPossibleScreen.DetectScreenByImage())
            {
                Form_Main.LogInConsole("Next screen found");
                Form_Main.LogInConsole(screen.Name);
                screen.nextPossibleScreen.PerformActionOnTheScreen();
            }
        }

    }
}

    

