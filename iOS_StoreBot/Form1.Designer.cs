﻿namespace iOS_StoreBot
{
    partial class Form_Main
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.contextMenuStrip2 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.btnTestFunction = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnContinue = new System.Windows.Forms.Button();
            this.labelThreadState = new System.Windows.Forms.Label();
            this.btnStartBot = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioChoiceFigure = new System.Windows.Forms.RadioButton();
            this.radioApp1 = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxAppKeyword = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.textBoxID = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.PasswordBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.LoginBox = new System.Windows.Forms.TextBox();
            this.PortBox = new System.Windows.Forms.TextBox();
            IpBox = new System.Windows.Forms.TextBox();
            BotConsoleLog = new System.Windows.Forms.TextBox();
            labelId = new System.Windows.Forms.Label();
            CheckBoxVPN = new System.Windows.Forms.CheckBox();
            checkBoxSpoofer = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // contextMenuStrip2
            // 
            this.contextMenuStrip2.Name = "contextMenuStrip2";
            this.contextMenuStrip2.Size = new System.Drawing.Size(61, 4);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.btnTestFunction);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Location = new System.Drawing.Point(19, 191);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(258, 170);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Test Area";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(7, 78);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(226, 23);
            this.button2.TabIndex = 45;
            this.button2.Text = "Region Form Autofill";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnTestFunction
            // 
            this.btnTestFunction.Location = new System.Drawing.Point(7, 136);
            this.btnTestFunction.Name = "btnTestFunction";
            this.btnTestFunction.Size = new System.Drawing.Size(226, 23);
            this.btnTestFunction.TabIndex = 44;
            this.btnTestFunction.Text = "Test function";
            this.btnTestFunction.UseVisualStyleBackColor = true;
            this.btnTestFunction.Click += new System.EventHandler(this.button1_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(7, 107);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(228, 23);
            this.button4.TabIndex = 43;
            this.button4.Text = "Test Detection";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(7, 49);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(228, 23);
            this.button5.TabIndex = 1;
            this.button5.Text = "Login Install Aprovement";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(7, 20);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(228, 23);
            this.button6.TabIndex = 0;
            this.button6.Text = "Login from Account Button";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button3);
            this.groupBox2.Controls.Add(this.btnPause);
            this.groupBox2.Controls.Add(this.btnContinue);
            this.groupBox2.Controls.Add(this.labelThreadState);
            this.groupBox2.Location = new System.Drawing.Point(12, 367);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 86);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Thread Controll";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(7, 61);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 23);
            this.button3.TabIndex = 3;
            this.button3.Text = "■";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnPause
            // 
            this.btnPause.Location = new System.Drawing.Point(88, 61);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(75, 23);
            this.btnPause.TabIndex = 2;
            this.btnPause.Text = "II";
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnContinue
            // 
            this.btnContinue.Location = new System.Drawing.Point(169, 61);
            this.btnContinue.Name = "btnContinue";
            this.btnContinue.Size = new System.Drawing.Size(76, 23);
            this.btnContinue.TabIndex = 1;
            this.btnContinue.Text = "I>";
            this.btnContinue.UseVisualStyleBackColor = true;
            this.btnContinue.Click += new System.EventHandler(this.btnContinue_Click);
            // 
            // labelThreadState
            // 
            this.labelThreadState.AutoSize = true;
            this.labelThreadState.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.labelThreadState.Font = new System.Drawing.Font("Elephant", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelThreadState.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.labelThreadState.Location = new System.Drawing.Point(80, 26);
            this.labelThreadState.Name = "labelThreadState";
            this.labelThreadState.Size = new System.Drawing.Size(91, 21);
            this.labelThreadState.TabIndex = 0;
            this.labelThreadState.Text = "_________";
            this.labelThreadState.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // btnStartBot
            // 
            this.btnStartBot.Location = new System.Drawing.Point(19, 162);
            this.btnStartBot.Name = "btnStartBot";
            this.btnStartBot.Size = new System.Drawing.Size(144, 23);
            this.btnStartBot.TabIndex = 44;
            this.btnStartBot.Text = "Start Bot";
            this.btnStartBot.UseVisualStyleBackColor = true;
            this.btnStartBot.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton4);
            this.groupBox3.Controls.Add(this.radioButton3);
            this.groupBox3.Controls.Add(this.radioChoiceFigure);
            this.groupBox3.Controls.Add(this.radioApp1);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.textBoxAppKeyword);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.textBoxID);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.PasswordBox);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.LoginBox);
            this.groupBox3.Controls.Add(this.PortBox);
            this.groupBox3.Controls.Add(IpBox);
            this.groupBox3.Location = new System.Drawing.Point(3, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(304, 144);
            this.groupBox3.TabIndex = 47;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Input Settings";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Location = new System.Drawing.Point(239, 94);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(49, 17);
            this.radioButton4.TabIndex = 42;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "app4";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(165, 119);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(70, 17);
            this.radioButton3.TabIndex = 41;
            this.radioButton3.TabStop = true;
            this.radioButton3.Text = "app3.png";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioChoiceFigure
            // 
            this.radioChoiceFigure.AutoSize = true;
            this.radioChoiceFigure.Location = new System.Drawing.Point(165, 94);
            this.radioChoiceFigure.Name = "radioChoiceFigure";
            this.radioChoiceFigure.Size = new System.Drawing.Size(70, 17);
            this.radioChoiceFigure.TabIndex = 40;
            this.radioChoiceFigure.TabStop = true;
            this.radioChoiceFigure.Text = "app2.png";
            this.radioChoiceFigure.UseVisualStyleBackColor = true;
            this.radioChoiceFigure.CheckedChanged += new System.EventHandler(this.radioChoiceFigure_CheckedChanged);
            // 
            // radioApp1
            // 
            this.radioApp1.AutoSize = true;
            this.radioApp1.Checked = true;
            this.radioApp1.Location = new System.Drawing.Point(89, 94);
            this.radioApp1.Name = "radioApp1";
            this.radioApp1.Size = new System.Drawing.Size(70, 17);
            this.radioApp1.TabIndex = 39;
            this.radioApp1.TabStop = true;
            this.radioApp1.Text = "app1.png";
            this.radioApp1.UseVisualStyleBackColor = true;
            this.radioApp1.CheckedChanged += new System.EventHandler(this.radioSpaceQuest_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(11, 94);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(54, 13);
            this.label16.TabIndex = 38;
            this.label16.Text = "AppName";
            // 
            // textBoxAppKeyword
            // 
            this.textBoxAppKeyword.Location = new System.Drawing.Point(89, 66);
            this.textBoxAppKeyword.Name = "textBoxAppKeyword";
            this.textBoxAppKeyword.Size = new System.Drawing.Size(154, 20);
            this.textBoxAppKeyword.TabIndex = 37;
            this.textBoxAppKeyword.Text = "bitcoin";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(35, 73);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(48, 13);
            this.label14.TabIndex = 36;
            this.label14.Text = "Keyword";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 13);
            this.label5.TabIndex = 33;
            this.label5.Text = "Account Id";
            // 
            // textBoxID
            // 
            this.textBoxID.Location = new System.Drawing.Point(89, 117);
            this.textBoxID.Name = "textBoxID";
            this.textBoxID.Size = new System.Drawing.Size(56, 20);
            this.textBoxID.TabIndex = 32;
            this.textBoxID.Text = "0";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(190, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Password";
            // 
            // PasswordBox
            // 
            this.PasswordBox.Location = new System.Drawing.Point(193, 43);
            this.PasswordBox.Name = "PasswordBox";
            this.PasswordBox.Size = new System.Drawing.Size(50, 20);
            this.PasswordBox.TabIndex = 29;
            this.PasswordBox.Text = "alpine";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(127, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(33, 13);
            this.label3.TabIndex = 28;
            this.label3.Text = "Login";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Port";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(1, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 26;
            this.label1.Text = "IP";
            // 
            // LoginBox
            // 
            this.LoginBox.Location = new System.Drawing.Point(130, 43);
            this.LoginBox.Name = "LoginBox";
            this.LoginBox.Size = new System.Drawing.Size(57, 20);
            this.LoginBox.TabIndex = 25;
            this.LoginBox.Text = "root";
            // 
            // PortBox
            // 
            this.PortBox.Location = new System.Drawing.Point(89, 43);
            this.PortBox.Name = "PortBox";
            this.PortBox.Size = new System.Drawing.Size(32, 20);
            this.PortBox.TabIndex = 24;
            this.PortBox.TabStop = false;
            this.PortBox.Text = "22";
            // 
            // IpBox
            // 
            IpBox.Location = new System.Drawing.Point(4, 43);
            IpBox.Name = "IpBox";
            IpBox.Size = new System.Drawing.Size(79, 20);
            IpBox.TabIndex = 23;
            IpBox.Text = "192.168.0.1";
            // 
            // BotConsoleLog
            // 
            BotConsoleLog.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            BotConsoleLog.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            BotConsoleLog.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            BotConsoleLog.ForeColor = System.Drawing.SystemColors.Window;
            BotConsoleLog.Location = new System.Drawing.Point(12, 472);
            BotConsoleLog.Multiline = true;
            BotConsoleLog.Name = "BotConsoleLog";
            BotConsoleLog.ReadOnly = true;
            BotConsoleLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            BotConsoleLog.Size = new System.Drawing.Size(279, 390);
            BotConsoleLog.TabIndex = 45;
            // 
            // labelId
            // 
            labelId.AutoSize = true;
            labelId.Location = new System.Drawing.Point(98, 456);
            labelId.Name = "labelId";
            labelId.Size = new System.Drawing.Size(50, 13);
            labelId.TabIndex = 41;
            labelId.Text = "Curent Id";
            // 
            // CheckBoxVPN
            // 
            CheckBoxVPN.AutoSize = true;
            CheckBoxVPN.Checked = true;
            CheckBoxVPN.CheckState = System.Windows.Forms.CheckState.Checked;
            CheckBoxVPN.Location = new System.Drawing.Point(170, 163);
            CheckBoxVPN.Name = "CheckBoxVPN";
            CheckBoxVPN.Size = new System.Drawing.Size(45, 17);
            CheckBoxVPN.TabIndex = 49;
            CheckBoxVPN.Text = "Vpn";
            CheckBoxVPN.UseVisualStyleBackColor = true;
            CheckBoxVPN.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBoxSpoofer
            // 
            checkBoxSpoofer.AutoSize = true;
            checkBoxSpoofer.Checked = true;
            checkBoxSpoofer.CheckState = System.Windows.Forms.CheckState.Checked;
            checkBoxSpoofer.Location = new System.Drawing.Point(221, 163);
            checkBoxSpoofer.Name = "checkBoxSpoofer";
            checkBoxSpoofer.Size = new System.Drawing.Size(80, 17);
            checkBoxSpoofer.TabIndex = 50;
            checkBoxSpoofer.Text = "MGSpoofer";
            checkBoxSpoofer.UseVisualStyleBackColor = true;
            checkBoxSpoofer.CheckedChanged += new System.EventHandler(this.checkBoxSpoofer_CheckedChanged);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(319, 891);
            this.Controls.Add(checkBoxSpoofer);
            this.Controls.Add(CheckBoxVPN);
            this.Controls.Add(labelId);
            this.Controls.Add(groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.btnStartBot);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(BotConsoleLog);
            this.Name = "Form_Main";
            this.Text = "iOS_Bot 1.9.4";
            this.Load += new System.EventHandler(this.Form_Main_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Button btnPause;
        public System.Windows.Forms.Button btnContinue;
        private System.Windows.Forms.Label labelThreadState;
        private System.Windows.Forms.Button btnStartBot;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioChoiceFigure;
        private System.Windows.Forms.RadioButton radioApp1;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.TextBox textBoxAppKeyword;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label5;
        public System.Windows.Forms.TextBox textBoxID;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.TextBox PasswordBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.TextBox LoginBox;
        public System.Windows.Forms.TextBox PortBox;
        private System.Windows.Forms.Button btnTestFunction;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton4;
        public System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private static System.Windows.Forms.CheckBox checkBoxSpoofer;
        private static  System.Windows.Forms.CheckBox CheckBoxVPN;
        public  static System.Windows.Forms.TextBox IpBox;
        public static System.Windows.Forms.TextBox BotConsoleLog;
        public static System.Windows.Forms.Label labelId;
    }
}

