﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace iOS_StoreBot
{
    public enum Verbs
    {
        Will = 251,
        Wont = 252,
        Do = 253,
        Dont = 254,
        Iac = 255
    }

    public enum Options
    {
        Sga = 3
    }

    public class TelnetConnection
    {
        TcpClient _tcpSocket;
        Stream peer;
        
        StreamWriter wtr;

        protected int _timeOutMs = 5000;
        protected int _timeOutReadCommand = 30;

        private string _host;
        public string Host
        {
            get { return _host; }
            set { _host = value; }
        }
        public int _port = 6000;
        protected static TelnetConnection _instance;
        public static TelnetConnection Instance
        {
            get
            {
                if (_instance == null)
                {                    
                    _instance = new TelnetConnection(Form_Main.IP_ssh_server, 6000);                  
                }
                return _instance;
            }
        }     
        protected TelnetConnection(string hostname, int port)
        {
            Host = hostname;
            _port = port;
            ConnectToDevice();
        }
        public string Login(string username, string password, int loginTimeOutMs)
        {
            int oldTimeOutMs = _timeOutMs;
            _timeOutMs = loginTimeOutMs;
            string s = Read();
            if (!s.TrimEnd().EndsWith(":"))
                throw new Exception("Failed to connect : no login prompt");
            ExecuteAdbCommand(username);

            s += Read();
            if (!s.TrimEnd().EndsWith(":"))
                throw new Exception("Failed to connect : no password prompt");
            ExecuteAdbCommand(password);

            s += Read();
            _timeOutMs = oldTimeOutMs;
            return s;
        }
        public bool ConnectToDevice()
        {
            return ConnectToDevice(false);
        }
        public bool ConnectToDevice(bool flReadOnlyDefMacConfig)
        {
            flReadOnlyDefMacConfig = false;
            if (_tcpSocket != null && _tcpSocket.Connected)
            {
                _tcpSocket.Client.Disconnect(true);
            }
            var timeStart = DateTime.Now;
            do
            {             
                var ipHost = Host;

                if (String.IsNullOrEmpty(ipHost))
                {
                      Thread.Sleep(10000);

                    continue;
                }
                try
                {
                    if (_tcpSocket == null || !_tcpSocket.Connected)
                    {
                       _tcpSocket = new TcpClient(ipHost, _port);
                        peer = _tcpSocket.GetStream();
                    }
                }
                catch (Exception ex)
                {
                                        
                }
                if (_tcpSocket != null && !_tcpSocket.Connected)
                {
                        Thread.Sleep(5000);
                }
                if (_tcpSocket != null && _tcpSocket.Connected)
                    break;
                var timeSpan = DateTime.Now - timeStart;
                if (timeSpan.TotalSeconds > _timeOutReadCommand)
                {
                    return false;
                }
            } while (true);
            return true;

        }
        public void ExecuteAdbCommand(string cmd)
        {
          
            ExecuteAdbCommandWithResult(cmd);
        }
        public string ExecuteAdbCommandWithResult(string command)
        {
             string result = "";
            string resultCommand = "";
            if (_tcpSocket == null)
            {
                ConnectToDevice();
            }
            if (!_tcpSocket.Connected)
            {
                ConnectToDevice();
            }

            var timeStart = DateTime.Now;
            var timeSpan = DateTime.Now - timeStart;
            try
            {
                ReadTelnetData();
                
                wtr.WriteLine(command);
                wtr.Flush();

                do
                {
                    result = ReadTelnetData();
                    result = ParseAnswerTelnetCommand(result, command);
                    if (!String.IsNullOrEmpty(result))
                    {
                        resultCommand += result;
                        if (resultCommand.Contains("#"))
                            break;
                    }
                    timeSpan = DateTime.Now - timeStart;
                    if (timeSpan.TotalSeconds > 10)
                    {
                        if (!command.Contains("tar"))
                        {
                            return "";
                        }
                        else if (timeSpan.TotalSeconds > 30)
                        {
                            return "";
                        }
                    }
                    Thread.Sleep(500);
                } while (true);

                // if (ParseAnswerTelnetCommand(ref resultCommand, out s)) return s;
                var arrayresult = resultCommand.Split('\n');
                if (arrayresult.Length > 1)
                {
                    string resultCommandParse = "";
                    for (int i = 0; i < arrayresult.Length; i++)
                    {
                        if (!String.IsNullOrEmpty(arrayresult[i]) && !arrayresult[i].Contains("#"))
                        {
                            resultCommandParse += arrayresult[i] + "\n";
                        }
                    }

                    resultCommand = resultCommandParse;
                }
     
            }
            catch (Exception ex)
            {
                  //ConnectToDevice();
            }
            return resultCommand;
 
        }
        private static string ParseAnswerTelnetCommand(string resultCommand, string execCmd)
        {
            string result = "";
            resultCommand = resultCommand.Replace("\r", "");
            if (!String.IsNullOrEmpty(resultCommand))
            {

                var arrayresult = resultCommand.Split('\n');
                try
                {
                    if (arrayresult.Length > 2)
                    {
                        for (int i = 0; i < arrayresult.Length; i++)
                        {
                            if (!arrayresult[i].Contains(execCmd))
                            {
                                if (!String.IsNullOrEmpty(arrayresult[i]))
                                    result += arrayresult[i] + "\n";
                            }
                        }
                    }
                    else
                    {
                        result = resultCommand;
                    }

                }
                catch (Exception ex)
                {
                   
                }
            }
            return result;
        }
        public bool Write(string cmd)
        {
            try
            {
                var timeStart = DateTime.Now;
                if (_tcpSocket == null || !_tcpSocket.Connected)
                {
                    while (!ConnectToDevice())
                    {
                        Thread.Sleep(10000);
                        var timeSpan = DateTime.Now - timeStart;
                        if (timeSpan.TotalSeconds > _timeOutReadCommand)
                        {
                            return false;
                        }
                    }

                }
                byte[] buf = Encoding.ASCII.GetBytes(cmd.Replace("\0xFF", "\0xFF\0xFF"));
                _tcpSocket.GetStream().Write(buf, 0, buf.Length);
                if (!cmd.Contains("input"))
                    Thread.Sleep(2000);
                else
                {
                    Thread.Sleep(2000);

                }
                return true;
            }
            catch (Exception ex)
            {
                if (_tcpSocket != null) _tcpSocket.Close();
                _tcpSocket = null;
            }
            return false;
        }
        public string Read()
        {
            try
            {
                var timeStart = DateTime.Now;
                if (_tcpSocket == null || !_tcpSocket.Connected)
                {
                    while (!ConnectToDevice())
                    {
                        var timeSpan = DateTime.Now - timeStart;
                        Thread.Sleep(1000);
                        if (timeSpan.TotalSeconds > _timeOutReadCommand)
                        {
                            return "";
                        }
                    }
                }

                StringBuilder sb = new StringBuilder();
                ParseTelnet(sb);
                return sb.ToString();
            }
            catch (Exception ex)
            {
       
            }
            return "";
        }
        public bool IsConnected
        {
            get { return _tcpSocket.Connected; }
        }
        void ParseTelnet(StringBuilder sb)
        {
            while (_tcpSocket.Available > 0)
            {
                int input = _tcpSocket.GetStream().ReadByte();
                switch (input)
                {
                    case -1:
                        break;
                    case (int)Verbs.Iac:
                        // interpret as command
                        _tcpSocket.GetStream().ReadByte();

                        break;
                    default:
                        sb.Append((char)input);
                        break;
                }
                //Thread.Sleep(500);
            }
        }
        private string ReadTelnetData()
        {
            return ReadTelnetData(_tcpSocket);
        }
        private string ReadTelnetData(TcpClient client)
        {
            string result = "";
            try
            {
                byte[] buf = new byte[1024];
                while (true)
                {
                    if (client.Available > 0)
                    {
                        int readLen = client.GetStream().Read(buf, 0, buf.Length);
                        if (readLen == 0)
                        {
                            break;
                        }
                        string text = Encoding.ASCII.GetString(buf, 0, readLen);
                        result += text;
                        Console.Write(text);
                    }
                    else
                    {
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                  ConnectToDevice();
            }
            return result;
        }
        public void socketTap(string x, string y)
        {         
            string command = "1011010";
            string coordsCode;

            if (x.Length < 4) //06010
            {
                x = (Convert.ToInt32(x) * 10).ToString();
               if(x.Length <4)
                {
                    x = "0" + x;
                }
            }
            else // если 4 цифры
            {
                //x = "0" + (Convert.ToInt32(x)).ToString();
                x = (Convert.ToInt32(x)).ToString();
            }

            if (y.Length < 4)
            {
                y = (Convert.ToInt32(y) * 10).ToString();
                if (y.Length < 4)
                {
                    y = "0" + y;
                }

                coordsCode = (x) + "0" + (y) + "\r\n";
                command += coordsCode;
            }

            else 
            {
                y = (Convert.ToInt32(y)).ToString();
                coordsCode = (x) + (y) + "0" + "\r\n";
                command += coordsCode;
            }


            

          
            //command += 93 * 10 + "0" + 529 * 10 + "\r\n";
            //   byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");
            // 10 - TASK_PERFORM_TOUCH = 10
            // 1 - Постоянная константа.
            // 1 - TOUCH_DOWN = 1
            // 01 - fingerIndex - указательный палец.
            // 0 - разделитель.
            // 3500 - 350 = (координата * 10)
            // 0 - разелитель
            // 9200 - 920 - координата * 10

            // Send the data through the socket.  
            byte[] msg = Encoding.ASCII.GetBytes(command);
       
            peer.Write(msg, 0, msg.Length);
            peer.Flush();
            ReadData();      
           
            //socketUsleep(1000);

            //msg = Encoding.ASCII.GetBytes("1010010350009200\r\n");
            // 10 - TASK_PERFORM_TOUCH = 10
            // 1 - Постоянная константа.
            // 0 - TOUCH_UP = 0
            // 01 - fingerIndex - указательный палец.
            // 0 - разделитель.
            // 3500 - 350 = (координата * 10)
            // 0 - разелитель
            // 9200 - 920 - координата * 10
            command = "1010010";

            //coordsCode = (x) + "0" + (y) + "\r\n";

            command += coordsCode;

            msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);       
            peer.Flush();

        }
        // Перегрузка с переменной для задержки между touchDown и touchUp
        public void socketTap(string x, string y, int pause)
        {
            Form_Main.LogInConsole("Tap: X = " + x + " " + "Y = " + y);
            string command = "1011010";
            string coordsCode;

            if (x.Length < 4) //06010
            {
                x = (Convert.ToInt32(x) * 10).ToString();
                if (x.Length < 4)
                {
                    x = "0" + x;
                }
            }
            else // если 4 цифры
            {
                //x = "0" + (Convert.ToInt32(x)).ToString();
                x = (Convert.ToInt32(x)).ToString();
            }

            if (y.Length < 4)
            {
                y = (Convert.ToInt32(y) * 10).ToString();
                if (y.Length < 4)
                {
                    y = "0" + y;
                }

                coordsCode = (x) + "0" + (y) + "\r\n";
                command += coordsCode;
            }

            else
            {
                y = (Convert.ToInt32(y)).ToString();
                coordsCode = (x) + (y) + "0" + "\r\n";
                command += coordsCode;
            }

            // Send the data through the socket.  
            byte[] msg = Encoding.ASCII.GetBytes(command);

            peer.Write(msg, 0, msg.Length);
            peer.Flush();
            ReadData();

            socketUsleep(pause * 1000);

            command = "1010010";

            command += coordsCode;

            msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();

        }
        public void socketUsleep(float time)
        {
            {
                try
                {

                    string command = "18";
                    command += time * 1000000 + "\r\n";

                    // Send the data through the socket.  
                    byte[] msg = Encoding.ASCII.GetBytes(command);
                    peer.Write(msg, 0, msg.Length);
                    peer.Flush();
                    Thread.Sleep(Convert.ToInt32(time) + 1000);
                    ReadData();
                    // Thread.Sleep(500);      
                }
                 catch (Exception e)
                {

                }
            }
       
        }
        public List<string> socketFindImage(string imgPath)
        {
            Thread.Sleep(1000);
            string[] split1 = { "0", "0" };
            List<string> prevRes = new List<string>();
            List<string> res = new List<string>();
           // socketUsleep(2000);
            again:
           
            try
            {
                ReadData();
                string command = "21";
                command += imgPath + ";;4;;0.8;;0.8" + "\r\n";
                var msg = Encoding.ASCII.GetBytes(command);

                peer.Write(msg, 0, msg.Length);
                peer.Flush();
                
                string response = "";
                do
                {
                    Thread.Sleep(5000);
                    response = ReadData();                   
                }

                while (String.IsNullOrEmpty(response));

                var numbers = Regex.Split(response, @"\D+");

                foreach (var num in numbers)
                {
                    if (num != "0" && num != "00")
                    {
                        prevRes.Add(num);
                    }
                }

                if (prevRes.Count <= 1)
                {
                    prevRes.Add("false");
                    prevRes.Add("false");
                    if (prevRes[0] == "")
                    {
                        prevRes.Remove(prevRes[0]);
                    }
                }
                //res.Add(prevRes);

                res.Add(prevRes[0]);
                res.Add(prevRes[1]);
                return res;
            }

            catch (Exception e)
            {
                //       disconnectSocket();
                goto again;
            }
            return res;
        }
        public void socketTapMove(int x, int y)
        {
            // Instance.StartSocketClient(Screen.IP_ssh_server);
            ReadData();

            string command = "1012010";
            command += x * 10 + "0" + y * 10 + "\r\n";

            //   byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");
            // 10 - TASK_PERFORM_TOUCH = 10
            // 1 - Постоянная константа.
            // 2 - TOUCH_MOVE = 2
            // 01 - fingerIndex - указательный палец.
            // 0 - разделитель.
            // 3500 - 350 = (координата * 10)
            // 0 - разелитель
            // 9200 - 920 - координата * 10

            // Send the data through the socket.  
            byte[] msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();
            // Thread.Sleep(500);
            //  disconnectSocket();


        }
        public void socketSwipeDown(int x1, int y1, int x2, int y2, int dur, int steps)
        {
            // Instance.StartSocketClient(Screen.IP_ssh_server);
            //TouchDown
            string command = "1011010";
            command += x1 * 10 + "0" + y1 * 10 + "\r\n";
            var msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();

            var x = x1;
            var y = y1;

            var dX = (x2 - x1) / steps;
            var dY = (y2 - y1) / steps;
            var dSleep = (float)dur / (float)steps;

            for (int i = 0; i < steps; i++)
            {
                x += dX;
                y += dY;

                socketTapMove(x, y);
                socketUsleep(dSleep);


                //            sleep(duration / steps)
                //          touchUp(id, end[0], end[1])
            }

            //touch_up
            command = "1010010";
            command += x2 * 10 + "0" + y2 * 10 + "\r\n";
            msg = Encoding.ASCII.GetBytes(command);
            Thread.Sleep(3000); //**!
            peer.Write(msg, 0, msg.Length);
            peer.Flush();

            // disconnectSocket();
        }
        public void socketSwipeUp(int x1, int y1, int x2, int y2, int dur, int steps)
        {
            //TouchDown

            //   Instance.StartSocketClient(Screen.IP_ssh_server);

            string command = "1011010";
            command += x1 * 10 + "0" + y1 * 10 + "\r\n";
            var msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();

            var x = x2;
            var y = y2;

            var dX = (x1 - x2) / steps;
            var dY = (y1 - y2) / steps;
            var dSleep = (float)dur / (float)steps;

            for (int i = 0; i < steps; i++)
            {
                x += dX;
                y += dY;

                socketTapMove(x, y);
                socketUsleep(dSleep);
            }

            //touch_up
            command = "1010010";
            command += x2 * 10 + "0" + y2 * 10 + "\r\n";
            msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();
            //    disconnectSocket();
        }
        public void socketSwitchApp(string appBoundle)
        {
            try
            {              
                string command = "11";
                command += appBoundle + "\r\n";
                var msg = Encoding.ASCII.GetBytes(command);
                peer.Write(msg, 0, msg.Length);
                peer.Flush();

                //       disconnectSocket();
                
            }
            catch
            {
                //       disconnectSocket();
             
            }
        }
        public void socketInsertText(string text)
        {
            string command = "24 1;;";
            command += text + "\r\n";

            byte[] msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();
        }
        public void socketCmd(string cmd)
        {
            string command = "13";
            command += cmd + "\r\n";

            // 18 - TASK_USLEEP = 18
            // 1 - Постоянная константа.
            // Send the data through the socket.  
            byte[] msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();
        }
        public void socketRespring()
        {
            //Instance.StartSocketClient(Screen.IP_ssh_server);
            socketCmd("killall -9 SpringBoard");
            ConnectToDevice();
            //    disconnectSocket();
        }
        public List<string> GetRGB()
        {
            again:
            string[] split1 = { "0", "0" };
            List<string> prevRes = new List<string>();
            List<string> res = new List<string>();
            string command = "23 1;;";
            command += "\r\n";

            byte[] msg = Encoding.ASCII.GetBytes(command);
            peer.Write(msg, 0, msg.Length);
            peer.Flush();

            string response = "";
            do
            {
                Thread.Sleep(5000);
                response = ReadData();
            }

            while (String.IsNullOrEmpty(response));

            var numbers = Regex.Split(response, @"\D+");

            foreach (var num in numbers)
            {
                if (num != "0" && num != "00")
                {
                    prevRes.Add(num);
                }
            }

            if (prevRes.Count <= 1)
            {
                prevRes.Add("false");
                prevRes.Add("false");
                if (prevRes[0] == "")
                {
                    prevRes.Remove(prevRes[0]);
                }
            }
          
            res.Add(prevRes[0]);
            res.Add(prevRes[1]);
            return res;
        }
           
        
        public void ResetDeviceId()
        {
            //         Instance.StartSocketClient(Screen.IP_ssh_server);

            socketSwitchApp("com.navercorp.businessinsight.DeviceInfo");
            socketUsleep(2000);
            var img = socketFindImage("/private/var/storeimages/DeviceInfoBack.png");
            if (img.Contains("false"))
            {
                //Screen.Tap("22", "82");
                socketTap("35", "70");
            }
            socketUsleep(2000);
            socketTap("156", "228");
            Form_Main.LogInConsole("Device ID UUID has ben reseted.");
            //     disconnectSocket();
        }
        private string ReadData()
        {
            string result = "";
            byte[] bufferToRead = new byte[1024];
            if (_tcpSocket.Available > 0)
            {
                Console.WriteLine("Peer Can Read");

                int countReadByte = peer.Read(bufferToRead, 0, 1024);
                result = System.Text.Encoding.ASCII.GetString(bufferToRead, 0, countReadByte);                
            }
            return result; 
        }
        

    }
}

