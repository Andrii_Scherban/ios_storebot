﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using iOS_StoreBot.Detector;
using iOS_StoreBot.Detector.Screens;
using Newtonsoft.Json;
using Renci.SshNet;
using Screen = iOS_StoreBot.Detector.Screen;
using iPhoneWorker;
using iPhoneWorker.Interfaces;
using iPhoneWorker.Services;

namespace iOS_StoreBot
{
    public partial class Form_Main : Form
    {
        public Form_Main()
        {
            InitializeComponent();
        }

        public static string AppImg1 = "/private/var/storeimages/app1.png";
        public static string AppImg2 = "/private/var/storeimages/app2.png";
        public static string AppImg3 = "/private/var/storeimages/app3.png";
        public static string AppImg4 = "/private/var/storeimages/app4.png";
        public static string AppIcon = AppImg1;
        public static string IP_ssh_server;
        string _accountsJsonPath = Environment.CurrentDirectory + "\\accounts.json";
        string _coordsJsonPath = Environment.CurrentDirectory + "\\coords.json";
        string _coordsVpnIpPath = Environment.CurrentDirectory + "\\vpn_ip.json";
        public static string AppKeyword;
        public static bool Pause = false;
        public static Thread _thStartBotCycle;
        
        private void Form_Main_Load(object sender, EventArgs e)
        {
            AccountsLoadJson();
            CoordsLoadJson();
            VpnIpLoadJson();
        }

        public void AccountsLoadJson()
        {
            try
            {
                using (StreamReader r = new StreamReader(_accountsJsonPath))
                {
                    string json = r.ReadToEnd();
                    Screen.Accs = JsonConvert.DeserializeObject<List<Accounts.AppleAcc>>(json);

                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Can't get acess to Accounts.json file");
            }
        }
        public void CoordsLoadJson()
        {
            try
            {
                using (StreamReader r = new StreamReader(_coordsJsonPath))
                {
                    string json = r.ReadToEnd();
                    Screen.Coordinates = JsonConvert.DeserializeObject<List<Coordinates.Coords>>(json);

                }
            }
            catch (Exception e)
            {
                MessageBox.Show("Can't get acess to coords.json file");
            }
        }

        public void VpnIpLoadJson()
        {
            try
            {
                using (StreamReader r = new StreamReader(_coordsVpnIpPath))
                {
                    string json = r.ReadToEnd();
                    Screen.VpnIps = JsonConvert.DeserializeObject<List<VpnIp._VpnIp>>(json);
                }
            }

            catch (Exception e)
            {
                //MessageBox.Show(e.ToString());
                MessageBox.Show("Can't get acess to vpn_ip.json file");
            }
        }

        public static void LogInConsole(string logText)
        {
            IP_ssh_server = IpBox.Text;
            // FormBotView.BotConsoleLog.AppendText(DateTime.Now.ToShortTimeString() + " " + logText + "\n" + Environment.NewLine);
            BotConsoleLog.BeginInvoke(
                                   (MethodInvoker)(() => BotConsoleLog.AppendText(DateTime.Now.ToShortTimeString() + " " + logText + "\n" + Environment.NewLine)));

            string path = Environment.CurrentDirectory + "\\" + IP_ssh_server + ".txt";
            if (!File.Exists(path))
            {
                // Create a file to write to.
                StreamWriter sw = File.CreateText(path);
            }
            else
            {
                try
                {
                    File.AppendAllText(path, DateTime.Now.ToShortTimeString() + 
                        " " + logText + "\n" + Environment.NewLine);
                }
                catch (Exception e)
                {

                }
            }
        }

        private void BotConsoleLog_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            IP_ssh_server = IpBox.Text;
            Screen.accId = textBoxID.Text;
            labelThreadState.Text = "PLAYING";
            labelThreadState.BackColor = Color.Green;
            this.Text = IpBox.Text;
            AppKeyword = textBoxAppKeyword.Text.ToString();
            //StartSocketClient(Form_Main.IP_ssh_server);

            //
            // Screen.phase = 0;
            Screen.phase = 0;
            // Screen.phase = 2;
            //
            Phone phone = new Phone(IpBox.Text, "root", "alpine");
            IPhoneJobService phoneJobService = new PhoneJobService(phone);
            Screen.IP_ssh_server = IpBox.Text;

            var SocketInstance = TelnetConnection.Instance;
        

            //

            _thStartBotCycle = new Thread(delegate ()
            {
                while (true)
                {
                    if (Pause != true)
                    {
                       // SocketInstance.StartSocketClient(IpBox.Text);
                        ScreenDetector.Instance.DetectScreen();
                        SocketInstance.socketUsleep(2000);
                    }
                    else
                    {
                        SocketInstance.socketUsleep(5000);
                    }
                }
            })
            {
                IsBackground = true
            };
            _thStartBotCycle.Start();
        }

        private void radioSpaceQuest_CheckedChanged(object sender, EventArgs e)
        {
            AppIcon = AppImg1;
            textBoxAppKeyword.Text = "bitcoin";
        }
        private void radioChoiceFigure_CheckedChanged(object sender, EventArgs e)
        {
            AppIcon = AppImg2;
            textBoxAppKeyword.Text = "prom";
        }

        private void btnContinue_Click(object sender, EventArgs e)
        {
            Detector.Screen.Pause = false;
            Pause = false;
            labelThreadState.Text = "PLAYING";
            labelThreadState.BackColor = Color.Green;

            btnPause.Enabled = true;
            btnContinue.Enabled = false;
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            Detector.Screen.Pause = true;
            Pause = true;
            labelThreadState.Text = "PAUSED";
            labelThreadState.BackColor = Color.Red;
            btnPause.Enabled = false;
            btnContinue.Enabled = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            IP_ssh_server = IpBox.Text;

            Screen.accId = textBoxID.Text;
            labelThreadState.Text = "PLAYING";
            labelThreadState.BackColor = Color.Green;
            this.Text = IpBox.Text;
            AppKeyword = textBoxAppKeyword.Text.ToString();
            //
            Screen.phase = 0;
            //            
            {
                _thStartBotCycle = new Thread(delegate ()
                {
                    // while (true)
                    {
                        ScreenDetector.Instance.DetectScreen();
                    }
                })
                {
                    IsBackground = true
                };
                _thStartBotCycle.Start();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Screen.IP_ssh_server = IpBox.Text;
            /*
            Phone phone = new Phone(IpBox.Text, "root", "alpine");
            IPhoneJobService phoneJobService = new PhoneJobService(phone);        
            Screen.IP_ssh_server = IpBox.Text;         

           var SocketInstance = SynchronousSocketClient.Instance;
            SocketInstance.StartSocketClient(IpBox.Text);         
            int indexSwipes = 0;

            SocketInstance.socketInsertText("Hello World!");
            */

            IP_ssh_server = IpBox.Text;         
            TelnetConnection telnetConnection = TelnetConnection.Instance;

           var img = telnetConnection.socketFindImage(Screen.HomeAppStoreIconImg);

            telnetConnection.socketTap((img[0]), (img[1]));

            //Screen.FileZilaSetNewVpnProfile();
            //telnetConnection.socketTap(253, 480);
            //var spliter = Screen.Coordinates[0].Search_button_keyboard_tap.Split();
            //telnetConnection.socketTap((spliter[0]), (spliter[1]));
            // var img = telnetConnection.socketFindImage(Screen.OpenVpn_addBtn);            
            // telnetConnection.socketTap(Convert.ToInt32(img[0]), Convert.ToInt32(img[1]));

            Form_Main.LogInConsole("Add btn pushed");


            #region Test swipe & screenshot
            /*
            string dirScreen = Environment.CurrentDirectory + "\\" + IpBox.Text;
            if (!Directory.Exists(dirScreen))
            {
                Directory.CreateDirectory(dirScreen);
            }


            Thread th = new Thread(delegate ()
            {
            //string screenPath = (dirScreen);
            System.IO.DirectoryInfo di = new DirectoryInfo(dirScreen);

                while (true)
                {                
                    Form_Main.LogInConsole(DateTime.Now.ToString("h:mm:ss tt").ToString() + " " + "Swipe"  + " " + indexSwipes);
                   

                    SocketInstance.socketSwipeDown(350, 962, 350, 334, 3, 10);
                    Thread.Sleep(3000);
                    SocketInstance.socketSwipeUp(350, 962, 350, 334, 3, 10);
                    Thread.Sleep(10000);
                    string imgResult = SocketInstance.socketFindImage(Screen.AccountLabel);
                    Form_Main.LogInConsole("Image founded" + " " + imgResult);
                    Thread.Sleep(10000);

                    phoneJobService.ConnectPhone();
                    phoneJobService.MakeScreenshot();
                    phoneJobService.SaveLastScreenShot(dirScreen);
                    Thread.Sleep(5000);
                    


                    //  SocketInstance.socketSwipeUp(290, 955, 350, 466, 3, 1);
                    if (indexSwipes % 50 == 0 && indexSwipes != 0)
                  {
                         Form_Main.LogInConsole(" " + "RESPRING" + " ");
                        //Screen.Respring();
                       SocketInstance.socketCmd("killall -9 SpringBoard");
                        Thread.Sleep(10000);
                       SocketInstance.StartSocketClient(IpBox.Text);

                       // DeleteDirectorySSH(PhoneJobService._client, "/private/var/mobile/Media/DCIM/100APPLE");


                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                            DeleteDirectorySSH(PhoneJobService._client, "/private/var/mobile/Media/DCIM/100APPLE");
                        }                        

                    }
                    indexSwipes++;
                    phoneJobService.DisconnectPhone();
                }
            });

            th.IsBackground = true;
            th.Start();
            */
            #endregion
        }

        private static void DeleteDirectorySSH(SshClient client, string path)
        {

            client.RunCommand("cd LandingZone;rm -rf <" + path +">");
            
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            AppIcon = AppImg3;
            textBoxAppKeyword.Text = "movies online";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            _thStartBotCycle.Abort();           
        }
        private void button6_Click(object sender, EventArgs e)
        {
            Screen.IP_ssh_server = IpBox.Text;
            Screen.accId = textBoxID.Text;

            /*
            var accountFieldLoginCoords = Screen.FindByImage(Screen.AccountFieldLoginImg);
            var spliter = accountFieldLoginCoords.Split();
            Screen.Tap((spliter[0].Replace(Screen.pattern, string.Empty)),
               (spliter[1].Replace(Screen.pattern, string.Empty)));

            Screen.InsertText(Screen.Accs[Int32.Parse(Screen.accId)].Login.ToString());

            var accountFieldPasswordCoords = Screen.FindByImage(Screen.AccountFieldPasswordImg);
            spliter = accountFieldPasswordCoords.Split();
            Screen.Tap((spliter[0].Replace(Screen.pattern, string.Empty)),
               (spliter[1].Replace(Screen.pattern, string.Empty)));

            Screen.InsertText(Screen.Accs[Int32.Parse(Screen.accId)].Password.ToString());

            var accountSignIn = Screen.FindByImage(Screen.AccountSignInActive);
            spliter = accountSignIn.Split();
            Screen.Tap((spliter[0].Replace(Screen.pattern, string.Empty)),
               (spliter[1].Replace(Screen.pattern, string.Empty)));

            spliter = Screen.Coordinates[0].Account_continue_btn_RGB.Split();

            var rgbAfterLoginResult = Screen.GetRGB(Convert.ToInt32(spliter[0]), Convert.ToInt32(spliter[1]));
            if (rgbAfterLoginResult.Contains("255"))
            {
                spliter = Screen.Coordinates[0].Other_opportunities.Split();
                Screen.Tap(spliter[0], spliter[1]);
               
            }
            */
        }
        private void button5_Click(object sender, EventArgs e)
        {
            Screen.IP_ssh_server = IpBox.Text;
            Screen.accId = textBoxID.Text;
            var SocketInstance = TelnetConnection.Instance;
            SocketInstance.socketInsertText(Screen.Accs[Int32.Parse(Screen.accId)].Password.ToString());

        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            AppIcon = AppImg4;
            textBoxAppKeyword.Text = "dig jumper";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            IP_ssh_server = IpBox.Text;

            Screen.accId = textBoxID.Text;
            labelThreadState.Text = "PLAYING";
            labelThreadState.BackColor = Color.Green;
            this.Text = IpBox.Text;
            AppKeyword = textBoxAppKeyword.Text.ToString();
            Screen.IP_ssh_server = IpBox.Text;

            {
               Thread _thRegChangeByBtnThread = new Thread(delegate ()
                {
                    // while (true)
                    {
                        _19_RegionChange.WayToRegionChangeForm();
                       // Screen.wait(2000);
                       // _19_RegionChange.InsertFormData();
                    }
                })
                {
                    IsBackground = true
                };
                _thRegChangeByBtnThread.Start();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if(CheckBoxVPN.Checked == true)
            {
                Screen.VpnMode = true;
            }
            else
            {
                Screen.VpnMode = false;
            }
        }

        private void checkBoxSpoofer_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBoxVPN.Checked == true)
            {
                Screen.SpooferMode = true;
            }
            else
            {
                Screen.SpooferMode = false;
            }
        }
    }
    }


