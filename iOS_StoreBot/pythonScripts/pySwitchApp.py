from zxtouch.client import zxtouch
import time
from zxtouch.touchtypes import *

import sys

device = zxtouch(sys.argv[1])
#device = zxtouch("192.168.0.102");
device.accurate_usleep(100000)

def sleep(seconds):
    device.accurate_usleep(seconds * 1000000)

sleep(0.1)

def touchDown(id, x, y):
    device.touch(1, id, x, y)

def touchMove(id, x, y):
    device.touch(2, id, x, y)

def touchUp(id, x, y):
    device.touch(0, id, x, y)

device.switch_to_app(sys.argv[2])

device.disconnect()



