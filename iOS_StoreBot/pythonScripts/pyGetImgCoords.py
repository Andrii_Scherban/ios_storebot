from zxtouch.client import zxtouch
import time
import sys

device = zxtouch(sys.argv[1])
time.sleep(1.5)
result_tuple = device.image_match(sys.argv[2])
if result_tuple[0]:
    result_dict = result_tuple[1]
    if float(result_dict["x"]) != 0.00 and float(result_dict["y"]) != 0.00:
        print(result_dict["x"], result_dict["y"])
        device.disconnect()
    else:
        print("false")
        device.disconnect()
