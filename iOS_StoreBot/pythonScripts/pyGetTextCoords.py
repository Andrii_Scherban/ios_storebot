from zxtouch.client import zxtouch
import time

import sys

device = zxtouch(sys.argv[1])
#device = zxtouch("192.168.0.147");
device.accurate_usleep(100000)

"""Get text from a region

       Args:
           region: a tuple containing start_x, start_y, width, height of the region to ocr. Format: (x, y, width, height)
           custom_words: an array of strings to supplement the recognized languages at the word recognition stage.
           minimum_height: the minimum height of the text expected to be recognized, relative to the image height. The default value is 1/32
           recognition_level: a value that determines whether the request prioritizes accuracy or speed in text recognition. 0 means accurate. 1 means faster.
           languages: an array of languages to detect, in priority order.  Default: english. Use get_supported_ocr_languages() to get the language list.
           auto_correct: whether ocr engine applies language correction during the recognition process. 0 means no, 1 means yes
           debug_image_path: debug image path. If you DONT want the ocr engine to output the debug image, leave it blank

       Returns:
           Result tuple: (success?, error_message/return value)

           if the operation successes, the return value will be an array of texts in the region.
       """

def sleep(seconds):
    device.accurate_usleep(seconds * 1000000)

sleep(0.1)


device.ocr((sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5]), sys.argv[6], 1/32, 1, "english", 0, "")
device.disconnect()



