from zxtouch.client import zxtouch
import time
from zxtouch.touchtypes import *

import sys

device = zxtouch(sys.argv[1])
#device = zxtouch("192.168.0.106")

def sleep(seconds):
    device.accurate_usleep(seconds * 1000000)

sleep(0.1)

def touchDown(id, x, y):
    device.touch(1, id, x, y)

def touchMove(id, x, y):
    device.touch(2, id, x, y)

def touchUp(id, x, y):
    device.touch(0, id, x, y)

def swipe(id: int, start: tuple, end: tuple, duration: int, steps: int):
    x, y = start[0], start[1]
    touchDown(id, x, y)
    for i in range(steps):
        x += (end[0] - start[0]) / steps
        y += (end[1] - start[1]) / steps
        touchMove(id, x, y)
        sleep(duration/steps)
    touchUp(id, end[0], end[1])

swipe(1, (int(sys.argv[2]), int(sys.argv[3])), (int(sys.argv[4]), int(sys.argv[5])), 3, 10)
device.disconnect()