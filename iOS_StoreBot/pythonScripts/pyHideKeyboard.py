from zxtouch.client import zxtouch
import time
from zxtouch.touchtypes import *

import sys

device = zxtouch(sys.argv[1])
#device = zxtouch("192.168.0.125");
device.accurate_usleep(100000)

def sleep(seconds):
    device.accurate_usleep(seconds * 1000000)

sleep(0.1)

device.hide_keyboard();

device.disconnect()



