from zxtouch.client import zxtouch
import time
from zxtouch.touchtypes import *

import sys

device = zxtouch(sys.argv[1])
device.accurate_usleep(100000)

def sleep(seconds):
    device.accurate_usleep(seconds * 1000000)

sleep(0.1)

def touchDown(id, x, y):
    device.touch(1, id, x, y)

def touchMove(id, x, y):
    device.touch(2, id, x, y)

def touchUp(id, x, y):
    device.touch(0, id, x, y)

try:
 time.sleep(3)

 time.sleep(3)
 device.insert_text(sys.argv[2])
 time.sleep(3)
 device.disconnect()

except Exception as e:

    print(e.__class__)  # this will run if an error happens
    device.disconnect()
finally:

    print('pyInstallApprovement')  # this will be printed anyways
    device.disconnect()