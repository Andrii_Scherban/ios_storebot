﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using iOS_StoreBot.Detector;


namespace iOS_StoreBot.Net
{
    // connect & disconnect
    // SingleTone instance
    // tap swipe non static
    // disconnect public non static
    public class SynchronousSocketClient
    {
        private static SynchronousSocketClient _instanceSynchronousSocketClient;
        private Socket sender;
        //private TcpClient sender;

        public static SynchronousSocketClient Instance
        {
            get
            {
                if (_instanceSynchronousSocketClient == null)
                {
                    _instanceSynchronousSocketClient = new SynchronousSocketClient();
                }
                return _instanceSynchronousSocketClient;
            }
        }
        private SynchronousSocketClient()
        {
             StartSocketClient(Form_Main.IP_ssh_server);
        }
        public void StartSocketClient(string ipAddressPhone)
        {
            disconnectSocket();

            IPAddress ipAddress = IPAddress.Parse(ipAddressPhone);
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 6000);

            // Create a TCP/IP  socket.  
            sender = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);

            // Connect the socket to the remote endpoint. Catch any errors.  
            try
            {
                sender.Connect(remoteEP);


                Console.WriteLine("Socket connected to {0}",
                    sender.RemoteEndPoint.ToString());
            }
            catch (SocketException se)
            {
                Console.WriteLine("SocketException : {0}", se.ToString());
            }
        }
        public void disconnectSocket()
        {
            try
            {
                if (sender != null)
                {
                    sender.Shutdown(SocketShutdown.Both);
                    sender.Close();
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        /*
            public static void StartClient()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];

            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.  
                IPAddress ipAddress = IPAddress.Parse("192.168.0.123");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 6000);

                // Create a TCP/IP  socket.  
                Socket sender = new Socket(ipAddress.AddressFamily,
                    SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);


                    Console.WriteLine("Socket connected to {0}",
                        sender.RemoteEndPoint.ToString());

                    // Encode the data string into a byte array.  

                    //byte[] msg = Encoding.ASCII.GetBytes("18300000.0\r\n"); // Sleep task_type 18 duration 300000 microsec (30sec)
                    //byte[] msg = Encoding.ASCII.GetBytes("12lol;;lolconent;;300000.0\r\n"); //show alert task_type 12 
                    // byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");  // SocketTap
                    byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");

                    socketTap(sender, 350, 962);

                    // Receive the response from the remote device.  

                    /*int bytesRec = sender.Receive(bytes);
                                        Console.WriteLine("Echoed test = {0}",
                                            Encoding.ASCII.GetString(bytes, 0, bytesRec));
                    //
        // Release the socket.  
        sender.Shutdown(SocketShutdown.Both); // Убрать после написания инстанса. *!!!
                    sender.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
        */
        public void socketTap(int x, int y)
        {
            //Instance.StartSocketClient(Screen.IP_ssh_server);

            string command = "1011010";
            command += x * 10 + "0" + y * 10 + "\r\n";

            //   byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");
            // 10 - TASK_PERFORM_TOUCH = 10
            // 1 - Постоянная константа.
            // 1 - TOUCH_DOWN = 1
            // 01 - fingerIndex - указательный палец.
            // 0 - разделитель.
            // 3500 - 350 = (координата * 10)
            // 0 - разелитель
            // 9200 - 920 - координата * 10

            // Send the data through the socket.  
            byte[] msg = Encoding.ASCII.GetBytes(command);
            int bytesSent = sender.Send(msg);

            socketUsleep(1000);

            command = "1010010";
            command += x * 10 + "0" + y * 10 + "\r\n";
            msg = Encoding.ASCII.GetBytes(command);


            //msg = Encoding.ASCII.GetBytes("1010010350009200\r\n");

            // 10 - TASK_PERFORM_TOUCH = 10
            // 1 - Постоянная константа.
            // 0 - TOUCH_UP = 0
            // 01 - fingerIndex - указательный палец.
            // 0 - разделитель.
            // 3500 - 350 = (координата * 10)
            // 0 - разелитель
            // 9200 - 920 - координата * 10

            bytesSent = sender.Send(msg);
            //disconnectSocket();
        }
        public void socketTapMove(int x, int y)
        {
           // Instance.StartSocketClient(Screen.IP_ssh_server);

            string command = "1012010";
            command += x * 10 + "0" + y * 10 + "\r\n";

            //   byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");
            // 10 - TASK_PERFORM_TOUCH = 10
            // 1 - Постоянная константа.
            // 2 - TOUCH_MOVE = 2
            // 01 - fingerIndex - указательный палец.
            // 0 - разделитель.
            // 3500 - 350 = (координата * 10)
            // 0 - разелитель
            // 9200 - 920 - координата * 10

            // Send the data through the socket.  
            byte[] msg = Encoding.ASCII.GetBytes(command);
            int bytesSent = sender.Send(msg);
            // Thread.Sleep(500);
          //  disconnectSocket();


        }       
        public void socketSwipeDown(int x1, int y1, int x2, int y2, int dur, int steps)
        {
           // Instance.StartSocketClient(Screen.IP_ssh_server);
            //TouchDown
            string command = "1011010";
            command += x1 * 10 + "0" + y1 * 10 + "\r\n";
            var msg = Encoding.ASCII.GetBytes(command);
            int bytesSent = sender.Send(msg);

            var x = x1;
            var y = y1;

            var dX = (x2 - x1) / steps;
            var dY = (y2 - y1) / steps;
            var dSleep = (float) dur / (float) steps;
            
            for (int i = 0; i < steps; i++)
            {
                x += dX;
                y += dY;

                socketTapMove(x, y);
                socketUsleep(dSleep);
               

                //            sleep(duration / steps)
                //          touchUp(id, end[0], end[1])
            }

            //touch_up
            command = "1010010";
            command += x2 * 10 + "0" + y2 * 10 + "\r\n";
            msg = Encoding.ASCII.GetBytes(command);
            Thread.Sleep(3000); //**!
            bytesSent = sender.Send(msg);

           // disconnectSocket();
        }
        public void socketSwipeUp(int x1, int y1, int x2, int y2, int dur, int steps)
        {
            //TouchDown

         //   Instance.StartSocketClient(Screen.IP_ssh_server);

            string command = "1011010";
            command += x1 * 10 + "0" + y1 * 10 + "\r\n";
            var msg = Encoding.ASCII.GetBytes(command);
            int bytesSent = sender.Send(msg);

            var x = x2;
            var y = y2;

            var dX = (x1 - x2) / steps;
            var dY = (y1 - y2) / steps;
            var dSleep = (float)dur / (float)steps;

            for (int i = 0; i < steps; i++)
            {
                x += dX;
                y += dY;

                socketTapMove(x, y);
                socketUsleep(dSleep);
            }

            //touch_up
            command = "1010010";
            command += x2 * 10 + "0" + y2 * 10 + "\r\n";
            msg = Encoding.ASCII.GetBytes(command);
            bytesSent = sender.Send(msg);
        //    disconnectSocket();
        }
        public void socketUsleep(float time)
        {
  
            // try
            {
                string command = "18";
                command += time * 1000000 + "\r\n";

                //   byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");
                // 18 - TASK_USLEEP = 18
                // 1 - Постоянная константа.


                // Send the data through the socket.  
                byte[] msg = Encoding.ASCII.GetBytes(command);
                int bytesSent = sender.Send(msg);
                // Thread.Sleep(500);      
            }
            /*
                catch (Exception e)
            {
                Console.WriteLine("socketUsleep error: Socket not connected.");
            }
            */
     //       disconnectSocket();
        }
        public void socketCmd(string cmd)
        {
      //      Instance.StartSocketClient(Screen.IP_ssh_server);

            string command = "13";
            command += cmd + "\r\n";

            //   byte[] msg = Encoding.ASCII.GetBytes("1011010350009200\r\n");
            // 18 - TASK_USLEEP = 18
            // 1 - Постоянная константа.


            // Send the data through the socket.  
            byte[] msg = Encoding.ASCII.GetBytes(command);
            int bytesSent = sender.Send(msg);
            // Thread.Sleep(500);      
        //    disconnectSocket();
        }
        public void socketRespring()
        {
            Instance.StartSocketClient(Screen.IP_ssh_server);

            socketCmd("killall -9 SpringBoard");
       //    disconnectSocket();
        }
        public List<string> socketFindImage(string imgPath)
        {
            Thread.Sleep(3000);
            string[] split1 = {"0", "0"};
            List<string> prevRes = new List<string>();
            List<string> res = new List<string>();
            socketUsleep(2000);
            again:
            //disconnectSocket();
            //StartSocketClient(Screen.IP_ssh_server);

            //   Instance.StartSocketClient(Screen.IP_ssh_server);
            //ConnectPhone();
            try
            {
                string command = "21";
                command += imgPath + ";;4;;0.8;;0.8" + "\r\n";
                var msg = Encoding.ASCII.GetBytes(command);               
                 
               
                
                int bytesSent = sender.Send(msg);
                byte[] bytes = new byte[1024];
                int bytesRec = sender.Receive(bytes);
                var response = System.Text.Encoding.ASCII.GetString(bytes, 0, bytesRec);
               
                var numbers = Regex.Split(response, @"\D+");
                //List<string> strl = new List<string>();

                //split1 = response.Replace(";", " ").Split(' ');

                foreach (var num in numbers)
                {
                    if (num != "0" && num != "00")
                    {
                        prevRes.Add(num);
                    }
                }
              
                if (prevRes.Count <= 1)
                {
                    prevRes.Add("false");
                    prevRes.Add("false");
                    if(prevRes[0] == "")
                    {
                        prevRes.Remove(prevRes[0]);
                    }
                }

          

                //res.Add(prevRes);

                res.Add(prevRes[0]);
                res.Add(prevRes[1]);
                return res;
            }             
            
            catch (Exception e)
            {
                //       disconnectSocket();
                goto again;
            }
            return res;
        }
        public string socketSwitchApp(string appBoundle)
        {
            try
            {
          //      Instance.StartSocketClient(Screen.IP_ssh_server);

                string command = "11";
                command += appBoundle + "\r\n";
                var msg = Encoding.ASCII.GetBytes(command);
                int bytesSent = sender.Send(msg);
                byte[] bytes = new byte[1024];
                int bytesRec = sender.Receive(bytes);
                string result = System.Text.Encoding.ASCII.GetString(bytes, 0, bytesRec);
         //       disconnectSocket();
                return result;
            }
            catch
            {
         //       disconnectSocket();
                return "false";
            }
        } 
        public void socketInsertText(string text)
        {
    //        Instance.StartSocketClient(Screen.IP_ssh_server);

            string command = "24 1;;";
            command += text + "\r\n";

            byte[] msg = Encoding.ASCII.GetBytes(command);
            int bytesSent = sender.Send(msg);

     //       disconnectSocket();
        }
        public string socketGetRgb(int x, int y)
        {
       //     Instance.StartSocketClient(Screen.IP_ssh_server);

            string result = "0";
        //    disconnectSocket();
            return result;
        }
        public void ResetDeviceId()
        {
   //         Instance.StartSocketClient(Screen.IP_ssh_server);

            socketSwitchApp("com.navercorp.businessinsight.DeviceInfo");
            socketUsleep(2000);
            var img = socketFindImage("/private/var/storeimages/DeviceInfoBack.png");
            if (img.Contains("false"))
            {
                //Screen.Tap("22", "82");
                socketTap(35, 70);
            }
            socketUsleep(2000);
            socketTap(156, 228);
            Form_Main.LogInConsole("Device ID UUID has ben reseted.");
       //     disconnectSocket();
        }

      
    }
}
